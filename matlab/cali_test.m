robot = importrobot('urdf/ur10_urdf/ur10_robot.urdf');

T = SE3(getTransform(robot, to_rtb_q([0.5197410583496094, -0.562946156864502, 1.3491633574115198, -2.3341175518431605, -1.5721543470965784, -0.9364593664752405], 0), 'ee_link', 'base_link'))

tform2eul(T.double())