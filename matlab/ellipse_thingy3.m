clear all;

syms d r pi alp;

a = d/(2*cos(alp))
la = (a+r)*(pi-2*alp)
l = 2*la+4*alp*r
simplify(l)

double(subs(l,          {pi,d,r,alp},{3.1415,0.4,0.03,deg2rad(0)}))
double(subs(2*pi*r+d*pi,{pi,d,r,alp},{3.1415,0.4,0.03,deg2rad(0)}))

double(subs(l,          {pi,d,r,alp},{3.1415,0.4,0.03,deg2rad(88)}))
double(subs(2*pi*r+d*2 ,{pi,d,r,alp},{3.1415,0.4,0.03,deg2rad(88)}))

double(subs(la ,{pi,d,r,alp},{3.1415,0.4 ,0.03,deg2rad(88)}))

%fplot(subs(la,{pi,d,r},{3.1415,0.4,0.03}),[0 deg2rad(90)])
fplot(subs(l,{pi,d,r},{3.1415,0.4,0.03}),[0 deg2rad(90)])

2*0.4+4*deg2rad(88)*0.03