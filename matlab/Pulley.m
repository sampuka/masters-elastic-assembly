classdef Pulley
    %PULLEY Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        T,
        radius,
        height
    end
    
    methods
        function obj = Pulley(T_, radius_, height_)
            %UNTITLED Construct an instance of this class
            %   Detailed explanation goes here
            obj.T = SE3(T_);
            obj.radius = radius_;
            obj.height = height_;
        end
    end
end

