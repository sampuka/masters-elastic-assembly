clear all;
syms r d l pi;

%solve([a*0^2+b*0+c == 2*pi*r + 2*d; a*(pi/2)^2+b*(pi/2)+c == 2*pi*r + pi*d; 2*a*(pi/2)+b == 0]', [a b c])

%alp = 1/2*pi*(1+sqrt(d*(2*(pi-1)*d-l+2*pi*r))/(sqrt(pi-2)*d))-pi
alp = 1/2*pi*(1-sqrt(-d*(2*d-l+2*pi*r))/(sqrt(pi-2)*d))
vpa(subs(alp,pi,3.1415))

r = 0.03;
l = 1.5;
pi = 3.1415;

alp = subs(alp)

d_min = double(subs((l-2*pi*r)/pi))
d_max = double(subs((l-2*pi*r)/2))
double(subs(alp,d,d_min))
double(subs(alp,d,d_max))
fplot(alp, [d_min d_max])

double(2*pi*r+2*d_max)
double(2*pi*r+pi*d_min)

syms a

double(subs(d*(pi-2*a)/cos(a)+2*pi*r,{a,r,d},{deg2rad(0),  0.03, d_min}))
double(subs(d*(pi-2*a)/cos(a)+2*pi*r,{a,r,d},{deg2rad(89), 0.03, d_max}))

%pi/4-ans