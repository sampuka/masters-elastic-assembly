classdef MassSpringCable < handle
    %MASSSPRINGCABLE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        n,
        looped,
        l0,
        m,
        ks,
        kb,
        kt,
        kd,
        dt,
        x,
        xs,
        constraints,
        not_initialized
    end
    
    methods
        function obj = MassSpringCable(joint_count, diameter, length, mass, youngs, possion)
            %MASSSPRINGCABLE Construct an instance of this class
            %   Detailed explanation goes here
            obj.n = joint_count;
            obj.l0 = length/joint_count;
            obj.m = mass/joint_count;
            A = pi*diameter^2/4;
            obj.ks = youngs*A/obj.l0;
            I = pi*diameter^4/64;
            obj.kb = 3*youngs*I/obj.l0;
            G = youngs/2*(1+possion);
            I_p = pi*diameter^4/32;
            obj.kt = G*I_p/obj.l0;
            obj.kd = 0.5;
            obj.dt = sqrt(obj.m*obj.l0/obj.ks);
            obj.x = rand(obj.n, 3);
            obj.xs = zeros(obj.n, 3);
            obj.constraints = zeros(0, 4);
            obj.looped = false;
            obj.not_initialized = true;
            obj
        end
        
        function idx = index(obj, i)
            %INDEX Summary of this method goes here
            %   Detailed explanation goes here
            if i > obj.n
                idx = obj.index(i-obj.n);
            elseif i < 1
                idx = obj.index(i+obj.n);
            else
                idx = i;
            end
        end
        
        function constrain(obj, joint_number, pos)
            %CONSTRAIN Summary of this method goes here
            %   Detailed explanation goes here
            for i = 1:size(obj.constraints,1)
                if joint_number == obj.constraints(i,1)
                    obj.constraints(i, 2:4) = pos;
                    return
                end
            end
                    
            obj.constraints = [obj.constraints ; [joint_number pos]];
            obj.x(joint_number, :) = pos;
        end
        
        function acc = update(obj, dt)
            ddt = 0;
            while ddt < dt
                acc = obj.update_step;
                ddt = ddt + obj.dt;
            end
        end
        
        function acc = update_step(obj)
            %UPDATE Summary of this method goes here
            %   Detailed explanation goes here
            
            %if obj.looped
            %    size = obj.n;
            %else
            %    size = obj.n-1;
            %end
            
            %f = zeros(size, 2);
            %for i = 1:size
            %    f = [mod(i-2+obj.n, obj.n)+1 i];
            %end
            
            l_v = zeros(obj.n, 3);
            for i = 1:obj.n
                l_v(i,:) = obj.x(obj.index(i),:)-obj.x(obj.index(i-1),:);
            end
            l_v;
            
            u = zeros(obj.n, 3);
            for i = 1:obj.n
                u(i,:) = l_v(i,:)/norm(l_v(i,:));
            end
            u;
            
            l = zeros(1, obj.n);
            for i = 1:obj.n
                l(i) = norm(l_v(i,:));
            end
            l;
            
            b = zeros(1, obj.n);
            for i = 1:obj.n
                %l_v(obj.index(i+1),:)'
                %l_v(i,:)
                %mtimes(l_v(obj.index(i+1),:)',l_v(i,:))
%                 b(i) = ...
%                     arctan(...
%                         norm(...
%                             cross(...
%                                 l_v(obj.index(i+1),:),...
%                                 l_v(i,:)...
%                             )...
%                         ) / ...
%                         mtimes(l_v(obj.index(i+1),:)',l_v(i,:))...
%                     );
                if obj.looped || ((i ~= 1) && (i ~= obj.n))
                    b(i) = real(acos(dot(l_v(i,:),l_v(obj.index(i+1),:))/(norm(l_v(i,:))*norm(l_v(obj.index(i+1),:)))));
                end
            end
            b;
                
            E_s = zeros(1, obj.n);
            for i = 1:obj.n
                E_s(i) = obj.ks*(l(i) - obj.l0)^2;
            end
            %E_s;
            
            F_s = zeros(obj.n, 3);
            for i = 1:obj.n
                if (obj.looped || (i ~= 1))
                    F_s(i,:) = F_s(i,:) - obj.ks*(l(i)-obj.l0)*u(i,:);
                end
                if (obj.looped || (i ~= obj.n))
                    F_s(i,:) = F_s(i,:) + obj.ks*(l(obj.index(i+1))-obj.l0)*u(obj.index(i+1),:);
                end
            end
            
            F_b = zeros(obj.n, 3);
            for i = 1:obj.n
                if obj.looped || ((i ~= 1) && (i ~= obj.n))
                    if b(obj.index(i-1)) > 0.001
                        F_b(i,:) = F_b(i,:) + obj.kb*b(obj.index(i-1))/l(i)*cross(u(i,:), cross(u(obj.index(i-1),:), u(i,:)))/sin(b(obj.index(i-1)));
                    end
                    
                    if b(i) > 0.001
                        F_b(i,:) = F_b(i,:) - obj.kb*b(i)/l(i)*cross(u(i,:), cross(u(i,:), u(obj.index(i+1),:)))/sin(b(i));
                    end
                    
                    if b(i) > 0.001
                        F_b(i,:) = F_b(i,:) - obj.kb*b(i)/l(obj.index(i+1))*cross(u(obj.index(i+1),:), cross(u(i,:), u(obj.index(i+1),:)))/sin(b(i));
                    end
                    
                    if b(obj.index(i+1)) > 0.001
                        F_b(i,:) = F_b(i,:) + obj.kb*b(obj.index(i+1))/l(obj.index(i+1))*cross(u(obj.index(i+1),:), cross(u(obj.index(i+1),:), u(obj.index(i+2),:)))/sin(b(obj.index(i+1)));
                    end
                end
            end
            F_b
            
            F = F_s+F_b-obj.kd*obj.xs;
            
            %F_lim = 1000000;
            %for i = 1:obj.n
            %    nor = norm(F(i,:));
            %    if nor > F_lim
            %        F(i,:) = F(i,:)/nor*F_lim;
            %    end
            %end
            %obj.kd*obj.xs;
            %F;
            
            acc = F/obj.m;
            for i = 1:size(F_s,1)
                acc(i, :) = acc(i, :) + [0 0 -9.82];
            end
            
            obj.xs = obj.xs + acc   *obj.dt;
            obj.x  = obj.x  + obj.xs*obj.dt;
           
            for i = 1:size(obj.constraints,1)
                acc(obj.constraints(i,1),:) = 0;
                obj.xs(obj.constraints(i,1),:) = 0;
                obj.x(obj.constraints(i,1),:) = obj.constraints(i,2:4);
            end
            
            acc;
            obj.xs;
            obj.x;
        end
        
        function relax(obj, threshold)
            %RELAX Summary of this method goes here
            %   Detailed explanation goes here
            
            if obj.not_initialized
                obj.not_initialized = false;
                if size(obj.constraints,1) == 1
                    obj.x = obj.constraints(1,2:4)+rand(obj.n, 3)*obj.l0;
                elseif size(obj.constraints,1) > 1
                    for c = 1:size(obj.constraints,1)
                        A = obj.constraints(c,1);
                        c2 = c+1;
                        if c2 > size(obj.constraints,1)
                            c2 = 1;
                        end
                        B = obj.constraints(c2,1);
                        if B < A
                            B = B + obj.n;
                        end

                        for i = A+1:B-1
                            obj.x(obj.index(i),:) = obj.constraints(c,2:4)+(obj.constraints(c2,2:4)-obj.constraints(c,2:4))*(i-A)/(B-A);
                        end
                    end
                end
            end
            
            %h = plot3(obj.x(:,1), obj.x(:,2), obj.x(:,3))
            %xlim([-0.5 0.5])
            %ylim([-0.5 0.5])
            %zlim([-5.5 1])
            %pause(2)
            thr = 99999999;
            while thr > threshold
                disp(thr);
                acc = obj.update_step;
                thr = 0;
                for i = 1:obj.n
                    thr = thr + norm(acc(i,:)) + norm(obj.xs(i,:));
                end
                thr = thr/obj.n;
                %set(h, 'XData', obj.x(:,1), 'YData', obj.x(:,2), 'ZData', obj.x(:,3));
                %pause(obj.dt);
            end
            %hold off
        end
    end
end

