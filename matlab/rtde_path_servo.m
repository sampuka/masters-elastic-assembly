function rtde_path_servo(path)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    p = path;
    
    N = size(p,1);
    
    p2 = zeros(N,11);
    
    p2(1,1:6) = p(1,1:6);
    p2(1,7:11) = [0 0 4 0.1 100];
    
    v = 0.3;
    for i = 2:N
        dist = norm(p(i,1:6)-p(i-1,1:6));
        p2(i,1:6) = p(i,1:6);
        p2(i,7:11) = [0 0 dist/v 0.1 100];
    end    
    
    arg_str = "";
    for i = 1:N
        arg_str = strcat(arg_str, sprintf(' rtde_c.servoJ([%f, %f, %f, %f, %f, %f], %f, %f, %f, %f, %f); time.sleep(%f);', p2(i,:), p2(i,9)));
    end
    
    %for i = 1:N
    %    arg_str = sprintf('[%f, %f, %f, %f, %f, %f], %f, %f, %f, %f, %f', p2(i,:));

    raw_cmd = 'python3 -c ''import rtde_control; import time; rtde_c = rtde_control.RTDEControlInterface("192.168.10.10");%s'' ';

    cmd_string = sprintf(raw_cmd, arg_str);

    system(cmd_string)
    %end
end

