function T = table_cali()
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

    p = [
        -0.81309, -0.66788, -0.01497;
        -0.82000,  0.11815, -0.01480;
         0.36418, -0.66625, -0.01248;
         0.37139, -0.17603, -0.01356
        ];
    
    p_ = [
        0.0 0.0 0.0;
        0.0 0.8 0.0;
        1.2 0.0 0.0;
        0.9 0.8 0.0
        ];
    
    assert(size(p,1) == size(p_,1));
    N = size(p,1);
    
    % Floats the robot 15cm above table
    for i = 1:N
        %p_(i,3) = p_(i,3) - 0.15;
    end
     
    
    Cp = 0;
    Cp_ = 0;
    for i = 1:N
        Cp = Cp + p(i,:);
        Cp_ = Cp_ + p_(i,:);
    end
    Cp = Cp/N;
    Cp_ = Cp_/N;
    
    q = p;
    q_ = p_;
    for i = 1:N
        q(i,:) = q(i,:) - Cp;
        q_(i,:) = q_(i,:) - Cp_;
    end
    
    H = zeros(3,3);
    for i = 1:N
        H = H + q(i,:)*(q_(i,:))';
    end
    
    [U,Sigma,V] = svd(H);
    disp(Sigma)
    
    R = V*U';
    t = Cp_' - R*Cp';
    
    transl(t)*rotm2tform(R)
end

