clc;
clear;
close all;

%[robot, robot_config] = robot_init;
mdl_ur5;
robot_config = [0 0 0 0 0 0];

joint_count = 15;

cable_diameter = 0.005;
cable_length = 2;
cable_mass = 0.5;
cable_youngs = 20*10^6;
cable_poisson = 0.3;

cable = MassSpringCable(joint_count, cable_diameter, cable_length, cable_mass, cable_youngs, cable_poisson);
%cable.looped = true;
cable.constrain(1, [0.30 0.30 0]);
cable.constrain(joint_count, ur5.fkine([0 0 0 0 0 0]).t');
%cable.constrain(joint_count, [0.3*sin(0) 0.3*cos(0) 0.5]);
%cable.constrain(joint_count, [0.3+sin(0)*0.05 0.3+cos(0)*0.05 0.5]);
cable.relax(5);

ur5.plot(robot_config);
h = plot3(cable.x(:,1), cable.x(:,2), cable.x(:,3));
hold on;

%pause(10000000)

t = 0;
dt = 0.1;
going = true;
while going
    t = t+dt;
    
    q = [0 0 0 0 0 0];
    q(1) = mod(2*pi*t/20, 2*pi);
    q(2) = mod(2*pi*t/15, 2*pi);
    q(3) = mod(2*pi*t/12, 2*pi);
    q(4) = mod(2*pi*t/4, 2*pi);
    q(5) = mod(2*pi*t/5, 2*pi);
    
    %for qi = 1:6
    %    robot_config(qi).JointPosition = q(qi);
    %end
    robot_config = q;
    
    %show(robot, robot_config, 'PreservePlot', false, 'Frames', 'off');
    %cable.constrain(joint_count, [0.3*sin(t) 0.3*cos(t) 0.5]);
    %cable.constrain(joint_count, [0.3*sin(t) 0.3*cos(t) 0.5]);
    cable.constrain(joint_count, ur5.fkine(robot_config).t');
    %cable.relax(20);
    cable.update(0.01);
    %h = plot3(cable.x(:,1), cable.x(:,2), cable.x(:,3))
    set(h, 'XData', cable.x(:,1), 'YData', cable.x(:,2), 'ZData', cable.x(:,3));
    %plot2(cable.x);
    ur5.plot(robot_config);
    pause(dt);
    
    if t >= 30
        going = false;
    end
    t
end