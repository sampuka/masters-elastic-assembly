function rtde_gripper(mm)
    raw_cmd = 'python3 -c ''from robotiq_gripper_control import RobotiqGripper; from rtde_control import RTDEControlInterface; import time; rtde_c = RTDEControlInterface("192.168.10.10"); gripper = RobotiqGripper(rtde_c); gripper.move(%.0f);'' ';
    cmd = sprintf(raw_cmd, mm);
    system(cmd);
end
