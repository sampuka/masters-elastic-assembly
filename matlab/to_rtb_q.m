%% Vector q to RTB q (specific for the urdf file used)
function rtb_q = to_rtb_q(q, fingers)
    rtb_q = struct('JointName', {'shoulder_pan_joint' 'shoulder_lift_joint' 'elbow_joint' 'wrist_1_joint' 'wrist_2_joint' 'wrist_3_joint' 'gripper_finger_left_joint' 'gripper_finger_right_joint'}, 'JointPosition', {0 0 0 0 0 0 0 0});
    for i = 1:6
        rtb_q(i).JointPosition = q(i);
    end
    rtb_q(7).JointPosition = fingers;
    rtb_q(8).JointPosition = fingers;
end
