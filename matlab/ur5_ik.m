function [qs] = ur5_ik(T06)
%UR5_IK Analytic inverse kinematics for UR5.
%   qs = ur5_ik(T) finds the configurations that satisfy a desired end 
%   effector position. Takes a 4x4 homogenous transformation matrix of the
%   end effector and returns a Nx6 matrix where each row is a valid
%   configuration. (Typically N=8)

    theta = zeros(8,6);
    valid = [1 1 1 1 1 1 1 1];
   
    %% UR5 DH Parameters
    alpha = [0 deg2rad(90) 0 0 deg2rad(90) deg2rad(-90)];

    a = [0 -0.425 -0.39225 0 0 0];

    d = [0.089159 0 0 0.10915 0.09465 0.0823];
    
    %% URDF 180 degree rotation detail?
    T06 = trotz(deg2rad(180))*T06;

    %% Theta 1
    P05 = T06*[0; 0; -d(6); 1];
    P05 = P05(1:3);

    phi1 = atan2(P05(2), P05(1));
    phi2(1) =  acos(d(4)/norm(P05(1:2)));
    phi2(2) = -acos(d(4)/norm(P05(1:2)));

    for i = 1:4
        theta(i,1) = phi1 + phi2(1) + pi/2;
    end
    for i = 5:8
        theta(i,1) = phi1 + phi2(2) + pi/2;
    end

    for i = 1:8
        if theta(i,1)<-pi
            theta(i,1) = theta(i,1) + 2*pi;
        end
        if theta(i,1)>pi
            theta(i,1) = theta(i,1) - 2*pi;
        end
    end

    %% Theta 5
    P06 = T06(1:3,4);

    for i = 1:8
        theta(i,5) =  acos((P06(1)*sin(theta(i,1))-P06(2)*cos(theta(i,1))-d(4))/d(6));

        if ismember(i, [3 4 7 8])
            theta(i,5) = -theta(i,5);
        end
    end

    %% Theta 6
    T60 = inv(T06);
    X60 = T60(1:3,1);
    Y60 = T60(1:3,2);

    for i = 1:8
        theta(i,6) = atan2((-X60(2)*sin(theta(i,1))+Y60(2)*cos(theta(i,1)))/sin(theta(i,5)),...
                           ( X60(1)*sin(theta(i,1))-Y60(1)*cos(theta(i,1)))/sin(theta(i,5)));
    end

    %% Theta 3 and 2
    for i = 1:8
        T01 = DHLink(alpha(1), a(1), d(1), theta(i,1));
        T45 = DHLink(alpha(5), a(5), d(5), theta(i,5));
        T56 = DHLink(alpha(6), a(6), d(6), theta(i,6));

        T14 = inv(T01)*T06*inv(T45*T56);
        P14xz = [T14(1,4); T14(3,4)];

        theta(i,3) = acos((norm(P14xz)^2-a(2)^2-a(3)^2)/(2*a(2)*a(3)));

        if rem(i,2) == 0
            theta(i,3) = -theta(i,3);
        end

        theta(i,2) = atan2(-P14xz(2), -P14xz(1)) - asin(-a(3)*sin(theta(i,3))/norm(P14xz));
    end

    %% Theta 4
    for i = 1:8
        T01 = DHLink(alpha(1), a(1), d(1), theta(i,1));
        T12 = DHLink(alpha(2), a(2), d(2), theta(i,2));
        T23 = DHLink(alpha(3), a(3), d(3), theta(i,3));
        T45 = DHLink(alpha(5), a(5), d(5), theta(i,5));
        T56 = DHLink(alpha(6), a(6), d(6), theta(i,6));

        T34 = inv(T01*T12*T23)*T06*inv(T45*T56);

        theta(i,4) = atan2(T34(2,1), T34(1,1));
    end
    
    qs = theta;
end

%% Forward Kinematics DH
function T = DHLink(alp, a, d, ang)
    T = [cos(ang),          -sin(ang),         0,        a
         sin(ang)*cos(alp), cos(ang)*cos(alp), -sin(alp) -sin(alp)*d
         sin(ang)*sin(alp), cos(ang)*sin(alp), cos(alp), cos(alp)*d
         0,                 0,                 0,        1];
end