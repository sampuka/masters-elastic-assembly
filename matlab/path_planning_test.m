clc;
clear;
close all;
%%

setup = 2;

global robot;
global pulleys;
global belt_length;

if setup == 1
    robot = importrobot('urdf/ur10_urdf/ur10_robot.urdf');
    cfg = robot.homeConfiguration;
    home_q = [0 -pi/2 pi/2 0 0 0];

    belt_length = 0.41;

    pulleys = [
        Pulley(transl(0.2375, 0.11, 0.05+0.0), 0.030, 0.015)
        Pulley(transl(0.1100, 0.11, 0.05+0.0), 0.015, 0.015)
        ];
elseif setup == 2
    robot = importrobot('urdf/ur10_urdf/ur10_robot_setup2_lowcol.urdf');
    cfg = robot.homeConfiguration;
    home_q = [pi/4 -pi/2 pi/2 0 0 0];

    belt_length = 0.415;

    pulleys = [
        Pulley(transl(0.2375, 0.11, 0.05+0.0), 0.030, 0.013)
        Pulley(transl(0.1100, 0.11, 0.05+0.0), 0.015, 0.013)
        ];
elseif setup == 3
    robot = importrobot('urdf/ur10_urdf/ur10_robot_setup2_lowcol.urdf');
    cfg = robot.homeConfiguration;
    home_q = [pi/4 -pi/2 pi/2 -pi/2 0 0];

    belt_length = 0.41;

    pos_x = 10; % 1 = corner position
    pos_y = 7; % 1 = corner position
    rot = 2;
    
    base_T = transl([pos_x*0.05 pos_y*0.05 0.05]);
    %rot_T = transl([0.0 0 0])*trotz(-1);
    rot_T = transl([0.4 0 0])*trotz(pi/2);
    p1_T = transl([0.1875 0.06 0]);
    p2_T = transl([0.0600 0.06 0]);
    
    pulleys = [
        Pulley(base_T*rot_T^rot*p1_T, 0.030, 0.015)
        Pulley(base_T*rot_T^rot*p2_T, 0.015, 0.015)
        ];
elseif setup == 4
    robot = importrobot('urdf/ur10_urdf/ur10_robot_setup2.urdf');
    cfg = robot.homeConfiguration;
    home_q = [pi/4 -pi/2 pi/2 0 0 0];

    belt_length = 0.45;

    pulleys = [
        Pulley(transl(0.2375, 0.11, 0.05+0.0), 0.030, 0.015)
        Pulley(transl(0.1100, 0.11, 0.05+0.0), 0.015, 0.015)
        Pulley(transl(0.1500, 0.17, 0.05+0.0), 0.014, 0.015)
        ];
end

pn = size(pulleys,1);

pulley_distance = 0.012;
attach_angle = pi/6;

global fingers_open;
global fingers_closed;
fingers_open = 0;
fingers_closed = 0.024;

belt_radius = belt_length/(2*pi);

global belt_pnts;
belt_pnt_count = 50;
belt_pnts = zeros(3, belt_pnt_count);

% World -> Base transformation
global T_world_base;
T_world_base = SE3(robot.getTransform(cfg, 'base_link', 'world'));

% EE -> Grasp TCP transformation
global T_ee_grasp;
T_ee_grasp = SE3(robot.getTransform(cfg, 'grasp_link', 'ee_link'));

% Grasp TCP -> sides of gripper
global T_grasp_left;
global T_grasp_right;
T_grasp_left = SE3(transl([0.013 0 0]));
T_grasp_right = SE3(transl([-0.013 0 0]));

robot.show(to_rtb_q(home_q, fingers_closed));
hold on;
SE3().plot();
global belt_h;
belt_h = plot3(belt_pnts(1,:), belt_pnts(2,:), belt_pnts(3,:));

for i = 1:size(pulleys,1)
    [Xc,Yc,Zc] = cylinder(pulleys(i).radius);
    Zc = Zc*pulleys(i).height;
    surf(Xc+pulleys(i).T.t(1), Yc+pulleys(i).T.t(2), Zc+pulleys(i).T.t(3)-pulleys(i).height/2);
end

global collision_env
global collision_env_inflated
collision_env = {};
collision_env_inflated = {};
for i = 1:size(pulleys,1)
    collision_env{i} = collisionCylinder(pulleys(i).radius, pulleys(i).height);
    collision_env_inflated{i} = collisionCylinder(pulleys(i).radius + 0.002, pulleys(i).height + 0.001);
    collision_env{i}.Pose = transl(pulleys(i).T.t);
    collision_env_inflated{i}.Pose = transl(pulleys(i).T.t);
end
%collision_env = {collisionCylinder(pulleys(1).radius, pulleys(1).height), collisionCylinder(pulleys(2).radius, pulleys(2).height)};
%collision_env{1}.Pose = transl(pulleys(1).T.t);
%collision_env{2}.Pose = transl(pulleys(2).T.t);

%collision_env_inflated = {collisionCylinder(pulleys(1).radius + 0.002, pulleys(1).height + 0.001), collisionCylinder(pulleys(2).radius + 0.002, pulleys(2).height + 0.001)};
%collision_env_inflated{1}.Pose = transl(pulleys(1).T.t);
%collision_env_inflated{2}.Pose = transl(pulleys(2).T.t);

diff_T = inv(SE3(transl(pulleys(2).T.t)))*(SE3(transl(pulleys(1).T.t)));
ang = atan2(diff_T.t(2),diff_T.t(1));

global Ft;
Ft = SE3(transl(pulleys(1).T.t)*trotz(pi/2+ang));
Ft.plot();

global Tps;
Tps = pulleys;
for i = 1:size(Tps,1)
    Tps(i).T = inv(Ft)*Tps(i).T;
end

%% Waypoints
wp3_T = SE3(transl([0 0 0.005]))*Ft*SE3(trotz(attach_angle-pi/2)*transl([-0.04 -pulleys(1).radius 0]));

wp2_T = wp3_T*SE3(trotz(-attach_angle));

wp1_T = wp2_T*SE3(transl([0 0 0.1]));

wp4_T = wp3_T*SE3(transl([-0.09 0 0]));

wp5_T = wp4_T*SE3(trotz(-pi/2));

last_tan = cc_tan(pulleys(pn), pulleys(1), 2);

%wp6_T = pulleys(2).T*SE3(trotz(ang+pi)*transl([-pulleys(2).radius-0.03 -pulleys(2).radius 0]));
wp6_T = SE3(transl(last_tan(1:3)+last_tan(4:6)/2+[0 0 Ft.t(3)])*trotz(ang+pi));
%wp6_T.plot;

wp1_T = wp1_T*SE3(trotx(pi));
wp2_T = wp2_T*SE3(trotx(pi));
wp3_T = wp3_T*SE3(trotx(pi));
wp4_T = wp4_T*SE3(trotx(pi));
wp5_T = wp5_T*SE3(trotx(pi));
wp6_T = wp6_T*SE3(trotx(pi));

wp_q = zeros(6,6);

wp_q(1,:) = my_ik(wp1_T, home_q);
wp_q(2,:) = my_ik(wp2_T, wp_q(1,:));
wp_q(3,:) = my_ik(wp3_T, wp_q(2,:));
wp_q(4,:) = my_ik(wp4_T, wp_q(3,:));
wp_q(5,:) = my_ik(wp5_T, wp_q(4,:));
wp_q(6,:) = my_ik(wp6_T, wp_q(5,:));
             
[wp5_fr_success, wp_q(5,:)] = constrain_FOR(wp_q(5,:), wp_q(5,:));
[wp6_fr_success, wp_q(6,:)] = constrain_FOR(wp_q(6,:), wp_q(6,:));

if ~wp5_fr_success
    %[err, lv_O] = task_error(wp_q(5,:));
    error('Bad wp5');
end
if ~wp6_fr_success
    %[err, lv_O] = task_error(wp_q(6,:));
    error('Bad wp6');
end

%% Test
total_results = zeros(0,7);
its_per_eps = 10;
for eps = [0.03 0.04 0.05 0.06 0.07]
    results = zeros(its_per_eps,6);
    for it = 1:its_per_eps
        tic;
        path6 = rrt_belt([wp_q(5,:) 0 0], [wp_q(6,:) 0 0], eps, 30000);
        pathgen_time = toc;
        path_okay = check_path(path6);
        if path_okay && size(path6,1)>0
            tic;
            path6_2 = shortcut_belt(path6, eps);
            pathsc_time = toc;
            results(it,:) = [size(path6,1)>0 path_okay pathgen_time path_length(path6) pathsc_time path_length(path6_2)];
        else
            results(it,:) = [size(path6,1)>0 path_okay pathgen_time 0 0 0];
        end
    end

    sr_it = mean(results(:,1));
    sr_ch = mean(results(:,2));
    pg_tm = mean(results(:,3));

    results2 = results(results(:,1)>0 & results(:,2)>0,:);

    pg_ln = mean(results2(:,4));
    sc_tm = mean(results2(:,5));
    sc_ln = mean(results2(:,6));
    sc_sd = std(results2(:,6));

    if size(results2,1) > 0
        total_results = [total_results; [sr_it sr_ch pg_tm pg_ln sc_tm sc_ln sc_sd]];
    else
        total_results = [total_results; [sr_it sr_ch pg_tm 0 0 0 0]];
    end
    
    results
    results2
end
total_results

%% qdist
function dist = qdist(qs)
    q = qs(1:6) .* [1 1 1 1 1 0.2];
    %q = qs(1:6) .* [1 1 1 1 1 1];
    
    dist = 0;
    for i = 1:6
        dist = dist + q(i)^2;
    end
    dist = sqrt(dist);
end

%% Show path utility function
function show_path(p, display_belt)
    %global fingers_closed;
    for i = 1:size(p,1)
        if display_belt
            [err, lv_O] = task_error(p(i,1:6));
            fprintf("Error at step %d = %d\n", i, norm(err));
            set_q(p(i,1:6), lv_O);
        else
            set_q(p(i,1:6), zeros(0));
        end
        
        pause(0.5);
    end
end

%% RTB q to vector q (specific for the urdf file used)
function q = from_rtb_q(rtb_q)
    q = [0 0 0 0 0 0];
    for i = 1:6
        q(i) = rtb_q(i).JointPosition;
    end
end

%% Set q for robot
function set_q(q, lv)
    %global robot;
    %global T_ee_grasp;
    global fingers_closed;
    global robot;
    %global belt_h;
    %global belt_pnts;
    update_belt_points(q, lv);
    robot.show(to_rtb_q(q, fingers_closed), 'PreservePlot', false, 'Frames', 'off');
    
    %T = my_fk(q);
    
    %SE3(T).plot;
    %SE3(T*T_ee_grasp).plot;
end

%% Forward Kinematics
function T = my_fk(q)
    global robot;
    %global T_ee_grasp;
    %T = SE3(trotz(pi)*ur5_fk(q))*T_ee_grasp;
    T = SE3(getTransform(robot, to_rtb_q(q, 0), 'grasp_link', 'world'));%*trotx(deg2rad(-90))*troty(deg2rad(90)));
    % I think the URDF file has a wrongly rotated end effector, so this is needed
end

%% Inverse Kinematics
function q = my_ik(T, q0)
    %ik = robotics.InverseKinematics('RigidBodyTree', robot);
    %weights = [1 1 1 1 1 1];
    %q = from_rtb_q(ik('ee_link', T.T, weights, to_rtb_q(q0)));
    
    global robot;
    global T_world_base;
    global T_ee_grasp;
    
    T = inv(T_world_base)*T*inv(T_ee_grasp);
    
    qs = ur10e_ik(T.double);
    
    for i = 1:size(qs,1)
        for j = 1:6
            if abs(qs(i,j)-q0(j)) > pi
                if qs(i,j) < 0
                    qs(i,j) = qs(i,j) + 2*pi;
                else
                    qs(i,j) = qs(i,j) - 2*pi;
                end
            end
        end
    end
    
    qd = zeros(8,1);
    for i = 1:size(qd,1)
        qd(i) = norm(qs(i,1:5) - q0(1:5));
    end
    
    best_dist = qd(1);
    best = 1;
    
    for i = 2:size(qd,1)
        dist = qd(i,:);
        if dist < best_dist
            best = i;
            best_dist = dist;
        end
    end
    
    q = qs(best,:);
    q0;
    qs;
    qd;
    best;
    q;
end

function alpha = belt_angle(d)
    l = 1.5;
    r = 0.03;
    alpha = 1.5708 - (1.4708*(-d*(2*d - l + 6.283*r))^(1/2))/d;
end

function update_belt_points(q, lv)
    global belt_h;
    global belt_pnts;
    global belt_length;
    pnt_count = size(belt_pnts,2);
    if size(lv,1) == 0 % Just robot grabbing
        lerped = linspace(0,2*pi, pnt_count);
        r = belt_length/(2*pi);
        T = my_fk(q)*SE3(transl([0 r 0]));
        for i = 1:pnt_count
            belt_pnts(1:3,i) = T.t + T.R*[cos(lerped(i))*r; sin(lerped(i))*r; 0];
        end
        set(belt_h, 'XData', belt_pnts(1,:), 'YData', belt_pnts(2,:), 'ZData', belt_pnts(3,:)); 
    else
        lc = size(lv,1);
        pnts = zeros(3, 100*lc);
        for l = 1:lc
            for i = 1:100
                pnts(1:3,i+(l-1)*100) = (lv(l,1:3) + lv(l,4:6)*i/100)';
            end
        end
        set(belt_h, 'XData', pnts(1,:), 'YData', pnts(2,:), 'ZData', pnts(3,:)); 
    end
end

function length = path_length(path)
    length = 0;
    for i = 1:size(path,1)-1
        length = length + qdist(path(i+1,:)-path(i,:));
    end
end

function path = my_rrt(startState, goalState, maxConnectionDistance)
    path = my_rrt_constrained(startState, goalState, maxConnectionDistance, [0 0 0 0 0 0], SE3());
end

function new_path = shortcut(path, eps)
    global robot;
    global fingers_closed;
    global collision_env_inflated;
    environment = collision_env_inflated;
    finger_state = [fingers_closed fingers_closed];
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
                
    new_path = path;
    
    length = path_length(new_path);
    start_length = length;
    fprintf("Path starts at length %f\n", start_length);
    
    max_iterations = 20;
    for i = 1:max_iterations
        n = size(new_path,1);
        a = 0;
        b = 0;
        while b < a+2
            a = randi([1 n-2]);
            b = randi([3 n]);
        end
        
        q1 = new_path(a,:);
        q2 = new_path(b,:);
        
        dist = qdist(q2-q1);
        
        n2 = 2+floor(dist/eps);
        
        if (path_length(new_path(a:b,:)) < dist+0.001) && (b-a+1 == n2)
            %fprintf('Not shorter, %d %d\n', b-a, n2);
            continue;
        end
        
        lerp_path = zeros(n2, 6);
        
%        if n2 == 1
%            lerp_path(1,:) = q1+(q2-q1)/2;
%        else
        for j = 1:n2
            lerp_path(j,:) = (q1*(n2-j)+q2*(j-1))/(n2-1);
        end
%        end
        
        shortcut_valid = true;
        
        for j = 1:n2
            if ~(stateValidator.isStateValid([lerp_path(j,1:6) finger_state]))
                shortcut_valid = false;
                break;
            end
        end
        
        if shortcut_valid
            new_path = [new_path(1:a-1,:); lerp_path; new_path(b+1:n,:)];
            new_length = path_length(new_path);
            if new_length < length
                length = new_length;
                fprintf("%d = %f (%f%%)\n", i, length, 100*(length-start_length)/start_length);
            end
        end
    end
end

function new_path = shortcut_belt(path, eps)
    global robot;
    global fingers_closed;
    global collision_env_inflated;
    environment = collision_env_inflated;
    finger_state = [fingers_closed fingers_closed];
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
                
    new_path = path;
    
    length = path_length(new_path);
    start_length = length;
    fprintf("Path starts at length %f\n", start_length);
    
    max_iterations = 1000;
    i2 = 0;
    for i = 1:max_iterations
        i2 = i2 + 1;
        if i2 > 200 
            fprintf("Stopping shortcutting due to no progress in 200 iterations\n");
            break;
        end
        n = size(new_path,1);
        a = 0;
        b = 0;
        while b < a+2
            a = randi([1 n-2]);
            b = randi([3 n]);
        end
        
        q1 = new_path(a,:);
        q2 = new_path(b,:);
        
        dist = qdist(q2-q1);
        
        n2 = 2+floor(dist/eps);
        
        if (path_length(new_path(a:b,:)) < dist+0.001) && (b-a+1 == n2)
            %fprintf('Not shorter, %d %d\n', b-a, n2);
            continue;
        end
        
        lerp_path = zeros(n2, 6);
        
        for j = 1:n2
            lerp_path(j,:) = (q1*(n2-j)+q2*(j-1))/(n2-1);
        end
        
        shortcut_valid = true;
        
        for j = 1:n2
            q_s = lerp_path(j,:);
            
            [constrain_success, q_s] = constrain_FOR(q_s, q_s);
            
            [~, lv_O] = task_error(q_s);

            % Check if belt collides with pulley
            constrain_success = constrain_success && check_belt_collision(lv_O, true);
            
            if ~(constrain_success && stateValidator.isStateValid([q_s finger_state]))
                shortcut_valid = false;
                break;
            else
                lerp_path(j,:) = q_s;
            end
        end
        
        if shortcut_valid
            new_path = [new_path(1:a-1,:); lerp_path; new_path(b+1:n,:)];
            new_length = path_length(new_path);
            if new_length < length
                length = new_length;
                fprintf("%d, %d = %f (%f%%)\n", i, i2, length, 100*(length-start_length)/start_length);
                i2 = 0;
            end
        end
    end
end

function new_path = partial_shortcut_belt(path, eps)
    global pulleys;
    global robot;
    global fingers_closed;
    global collision_env_inflated;
    environment = collision_env_inflated;
    finger_state = [fingers_closed fingers_closed];
    %eps = 0.05;
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
                
    new_path = path;
    
    length = path_length(new_path);
    start_length = length;
    fprintf("Path starts at length %f\n", start_length);
    
    max_iterations = 1000;
    i2 = 0;
    for i = 1:max_iterations
        i2 = i2 + 1;
        if i2 > 200
            fprintf("Stopping partial shortcutting due to no progress in 200 iterations\n");
            break;
        end
        n = size(new_path,1);
        a = 0;
        b = 0;
        while b < a+2
            a = randi([1 n-2]);
            b = randi([3 n]);
        end
        f = randi([1 6]);
        
        q1 = new_path(a,:);
        q2 = new_path(b,:);
        
        qdist = norm(q2-q1);
        
        n2 = 2+floor(qdist/eps);
        
        if (path_length(new_path(a:b,:)) < qdist+0.001) && (b-a+1 == n2)
            %fprintf('Not shorter, %d %d\n', b-a, n2);
            continue;
        end
        
        lerp_path = new_path(a:b,:);
        
        %for j = 1:n2
        %    lerp_path(j,:) = (q1*(n2-j)+q2*(j-1))/(n2-1);
        %end
        lerp_path(:,f) = linspace(q1(f),q2(f),b-a+1)';
        
        shortcut_valid = true;
        
        for j = 1:b-a+1
            q_s = lerp_path(j,:);
            
            [constrain_success, q_s] = constrain_FOR(q_s, q_s);
            
            [~, lv_O] = task_error(q_s);

            % Check if belt collides with pulley
            constrain_success = constrain_success && check_belt_collision(lv_O, true);
            
            if ~(constrain_success && stateValidator.isStateValid([q_s finger_state]))
                shortcut_valid = false;
                break;
            else
                lerp_path(j,:) = q_s;
            end
        end
        
        if shortcut_valid
            new_path = [new_path(1:a-1,:); lerp_path; new_path(b+1:n,:)];
            new_length = path_length(new_path);
            if new_length < length
                length = new_length;
                fprintf("%d, %d = %f (%f%%)\n", i, i2, length, 100*(length-start_length)/start_length);
                i2 = 0;
            end
        end
    end
end

function [err, lv_O] = task_error(qs)
    global Tps;
    global belt_length;
    global T_grasp_left;
    global T_grasp_right;
    global Ft;
    try
        Ts = my_fk(qs);
        Tt = inv(Ft)*Ts;
        Tt_l = Tt*T_grasp_left;
        Tt_r = Tt*T_grasp_right;

        lv = zeros(0,6);

        tmp = cp_tan(Tps(1), Tt_r.t, 2);        
        lv = [lv; [tmp(1:3)+tmp(4:6) -tmp(4:6)]];

        pj = 1;
        for j = 1:size(Tps,1)-1
            tan = cc_tan(Tps(j), Tps(j+1), 1);
            lin = Tt_l.t(1:2)' - tan(1:2);
            if atan2(tan(5),tan(4)) > atan2(lin(2), lin(1))
                pj = pj + 1;
                lv = [lv; tan];
            else
                break;
            end
        end        

        lv = [lv; cp_tan(Tps(pj), Tt_l.t, 1)];

        l = 0;
        for j = 1:size(lv,1)
            l = l + norm(lv(j,4:5)); 
        end

        l = l + norm(T_grasp_left.t-T_grasp_right.t);

        for j = 1:pj
            al = lv(j,4:5);
            bl = lv(j+1,4:5);

            av = atan2(al(2),al(1));
            bv = atan2(bl(2),bl(1));
            dv = wrapTo2Pi(av-bv);
            l = l + dv*Tps(j).radius;
        end

        bl = lv(1,1:3) + lv(1,4:6) - lv(pj+1, 1:3);
        bd  = bl/norm(bl);
        bd = [-bd(2) bd(1) 0];

        dl = l - belt_length;
        slack = belt_length*0.06;

        if dl > slack
            dl = dl - slack;
        elseif dl < -slack
            dl = dl + slack;
        else
            dl = 0;
        end

        %P = bd * dl / 2;
        b = norm(bl) / 2;
        c = (norm(lv(1,4:6))+norm(lv(pj,4:6)))/2;
        a = sqrt(c^2-b^2);
        P = bd * dl*a/norm([a b]) / 2;
        P(3) = Tt_l.t(3);

        Ttd = Tt.double()*trotx(pi);
        %Ts2 = Ts*SE3(trotx(pi));
        psi0 = atan2(Ttd(3,2), Ttd(3,3));
        theta0 = atan2(-Ttd(3,1), norm([Ttd(3,2) Ttd(3,3)]));
        phi0 = atan2(Ttd(2,1), Ttd(1,1));

        l1 = lv(1,4:5);
        lj = lv(pj+1,4:5);
        lg = Tt_r.t'-Tt_l.t';

        v1 = atan2(l1(2),l1(1));
        vj = atan2(lj(2),lj(1));
        vg = atan2(lg(2),lg(1));

        vh = (vj+v1)/2;

%        if v1 > vj
%            error('ff');
%        end
        
        if vg < vh-0.1
            phi_err = vg-(vh-0.1);
        elseif vg > vh+0.1
            phi_err = vg-(vh+0.1);
        else
            phi_err = 0;
        end
        phi_err = wrapToPi(phi_err);

%         if vg < v1
%             phi_err = vg-v1;
%         elseif vg > vj
%             phi_err = vg-vj;
%         else
%             phi_err = 0;
%         end

        %Tt2 = Ttd*trotz(-phi0);
        %psi2 = atan2(Tt2(3,2), Tt2(3,3));
        %theta2 = atan2(-Tt2(3,1), norm([Tt2(3,2) Tt2(3,3)]));

        %psi_err = -psi2;
        %theta_err = -theta2;

        %Ttx = Tt2*trotx(-psi0)*trotz(phi0);
        %Rx = Ttx(1:3,1:3);
        %dTx = Ft.double*inv(Ttd)*Ttx;

        %Tty = Ttd*troty(theta0);
        %dTy = Ft.double*Tty;
        %dRy = dTy(1:3,1:3);

        %x_err = rotm2eul(Ttd(1:3,1:3)) - rotm2eul(Ttx(1:3,1:3));
        %x_err = rotm2eul(Tt2(1:3,1:3)*rotz(-phi0)*rotx(-psi0)*rotz(phi0)) - rotm2eul(Tt2(1:3,1:3));
        %y_err = rotm2eul(Tt2(1:3,1:3)*rotz(-phi0)*roty(-theta0)*rotz(phi0)) - rotm2eul(Tt2(1:3,1:3));

        %O_err = [y_err(3)+x_err(3) y_err(2)+x_err(2) phi_err];
        %O_err = [psi2 theta2 phi_err];
        O_err = [0 0 phi_err];
        %[0 0 0] + rotm2eul(dRy)

        %set_q(qs,zeros(0));

        %Ttd2 = SE3(transl(Tt.t')*trotz(phi0/2+pi/2)*rotm2tform(Tt.R)).double;
        %psi = atan2(Ttd2(3,2), Ttd2(3,3));
        %theta = atan2(-Ttd2(3,1), norm([Ttd2(3,2) Ttd2(3,3)]));

        %psi_err = wrapToPi(psi);

        err = [P O_err];
        %err = [P -theta_err 0];
        %err = [P psi_err -theta_err 0];

        lv_O = lv;
        for j = 1:size(lv_O,1)
            lv_O(j,1:3) = (Ft.t + Ft.R*lv_O(j,1:3)')';
            lv_O(j,4:6) = (       Ft.R*lv_O(j,4:6)')';
        end
    catch
        err = [1000 1000 1000 1000 1000 1000];
        lv_O = zeros(0);
    end
end

function tl = cp_tan(C, P, n)
    P = P';
    Cx = C.T.t(1);
    Cy = C.T.t(2);
    Px = P(1);
    Py = P(2);
    r = C.radius;

    dx_ = Px-Cx;
    dy_ = Py-Cy;

    dxr = -dy_;
    dyr = dx_;

    d = norm([dx_ dy_]);

    if d < r
        error('inside');
    end

    rho = r/d;
    ad_ = rho^2;
    bd_ = rho*sqrt(1-rho^2);

    if n == 1
        Tx = Cx + ad_*dx_ + bd_*dxr;
        Ty = Cy + ad_*dy_ + bd_*dyr;
    elseif n == 2
        Tx = Cx + ad_*dx_ - bd_*dxr;
        Ty = Cy + ad_*dy_ - bd_*dyr;
    end

    t = [Tx Ty 0];

    tl = [t P-t];
end

function tl = cc_tan(C1, C2, n)
    a = C1.T.t(1);
    b = C1.T.t(2);
    c = C2.T.t(1);
    d = C2.T.t(2);
    r0 = C1.radius;
    r1 = C2.radius;

    if r0 == r1
        error('same radius tangent');
    end

    D = norm([c-a d-b]);

    xp = (c*r0-a*r1)/(r0-r1);
    yp = (d*r0-b*r1)/(r0-r1);

    if n == 1
        xt1 = (r0^2*(xp-a)-r0*(yp-b)*sqrt((xp-a)^2+(yp-b)^2-r0^2)) / ((xp-a)^2+(yp-b)^2) + a;
        yt1 = (r0^2*(yp-b)+r0*(xp-a)*sqrt((xp-a)^2+(yp-b)^2-r0^2)) / ((xp-a)^2+(yp-b)^2) + b;

        xt2 = (r1^2*(xp-c)-r1*(yp-d)*sqrt((xp-c)^2+(yp-d)^2-r1^2)) / ((xp-c)^2+(yp-d)^2) + c;
        yt2 = (r1^2*(yp-d)+r1*(xp-c)*sqrt((xp-c)^2+(yp-d)^2-r1^2)) / ((xp-c)^2+(yp-d)^2) + d;
    else
        xt1 = (r0^2*(xp-a)+r0*(yp-b)*sqrt((xp-a)^2+(yp-b)^2-r0^2)) / ((xp-a)^2+(yp-b)^2) + a;
        yt1 = (r0^2*(yp-b)-r0*(xp-a)*sqrt((xp-a)^2+(yp-b)^2-r0^2)) / ((xp-a)^2+(yp-b)^2) + b;

        xt2 = (r1^2*(xp-c)+r1*(yp-d)*sqrt((xp-c)^2+(yp-d)^2-r1^2)) / ((xp-c)^2+(yp-d)^2) + c;
        yt2 = (r1^2*(yp-d)-r1*(xp-c)*sqrt((xp-c)^2+(yp-d)^2-r1^2)) / ((xp-c)^2+(yp-d)^2) + d;
    end

    t1 = [xt1 yt1 0];
    t2 = [xt2 yt2 0];

    tl = [t1 t2-t1];
end

function [constrain_success, q_s] = constrain_FOR(q_r, q_near)
    global Ft;
    global robot;
    
    max_error = 0.001;
    
    q_s = q_r;
    
    [err, ~] = task_error(q_s);
    
    if norm(err) > 1000
        constrain_success = false;
        return;
    end
        
    constrain_success = norm(err) < max_error;

    FOR_ints = 0;
    while ~constrain_success && FOR_ints < 10 % First Order Refraction
        FOR_ints = FOR_ints + 1;
        J0_full = geometricJacobian(robot, to_rtb_q(q_s, 0), 'grasp_link');
        J0 = [J0_full(4:6,1:6); J0_full(1:3,1:6)];
        R_0_t = inv(Ft.R);
        Jt = [R_0_t zeros(3); zeros(3,3) R_0_t]*J0;
        ctheta = cos(err(5));
        cphi = cos(err(6));
        stheta = sin(err(5));
        sphi = sin(err(6));
        E = [eye(3) zeros(3); zeros(3) [cphi/ctheta sphi/ctheta 0; -sphi cphi 0; cphi*stheta/ctheta sphi*stheta/ctheta 1]];
        J = E*Jt;
        dx = err';
        dq = (pinv(J)*(dx*1))';
        q_s(1:6) = q_s(1:6) - dq;

        [err, ~] = task_error(q_s);

        if norm(err) < max_error
            constrain_success = true;
        end

        if (false && qdist(q_s-q_r) > qdist(q_r-q_near)*3) || norm(err) > 1000
            break;
        end
    end
end

function state_valid = check_belt_collision(lv_O, useinflated)
    %global pulleys;
    global collision_env;
    global collision_env_inflated;
    
    if useinflated
        pulleys = collision_env_inflated;
    else
        pulleys = collision_env;
    end
    
    state_valid = true;
        
    for p = 2:length(pulleys)
        pul = pulleys{p};
        Pa = lv_O(1, 1:3);
        Pb = Pa+lv_O(1, 4:6);
        Ti = inv(SE3(pul.Pose));
        Pa = (Ti.t + Ti.R*Pa')';
        Pb = (Ti.t + Ti.R*Pb')';
        lpnts = [linspace(Pa(1), Pb(1), 20); linspace(Pa(2), Pb(2), 20); linspace(Pa(3), Pb(3), 20)]';
        for i = 1:size(lpnts,1)
            lpnt = lpnts(i,:);
            if norm(lpnt(1:2)) < pul.Radius && lpnt(3) < pul.Length/2
                state_valid = false;
                break;
            end
        end
        if ~state_valid
            break;
        end
    end
end

function path = rrt_belt(startState, goalState, eps, maxNumIterations)
    global fingers_closed;
    %global Ft;
    %global pulleys;
    finger_state = [fingers_closed fingers_closed];
    startState(7:8) = finger_state;
    goalState(7:8) = finger_state;
    
    tree_dist = norm(goalState-startState);
    
    total_its = 0;
    
    global robot;
    global collision_env_inflated;
    environment = collision_env_inflated;
    %environment = {};
    %maxNumIterations = 15000;
    
    path = [];
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
    
    [start_err, lv_O] = task_error(startState);
    [goal_err, lv_O] = task_error(goalState);
    fprintf("Task error start state = %d\n", norm(start_err));
    fprintf("Task error goal state = %d\n", norm(goal_err));

    maxNumNodes = maxNumIterations + 1;
    TreeA = nav.algs.internal.SearchTree(startState, maxNumNodes);
    TreeB = nav.algs.internal.SearchTree(goalState, maxNumNodes);
    TreeA.setCustomizedStateSpace(stateValidator.StateSpace);
    TreeB.setCustomizedStateSpace(stateValidator.StateSpace);
    
    randState =...
        stateValidator.StateSpace.sampleUniform(maxNumIterations);
    
    q_limits = zeros(6,2);
    for i = 1:6
        q_limits(i,1) = min([startState(i) goalState(i)])-0.2;
        q_limits(i,2) = max([startState(i) goalState(i)])+0.2;
    end
    
    for k = 1 : maxNumIterations
        q_rand = randState(k, :);
        q_rand(7:8) = finger_state;
        
        for i = 1:0
            q_rand(i) = rand()*(q_limits(i,2)-q_limits(i,1))+q_limits(i,1);
        end

        %Extend the TreeA towards a random configuration
        nearId = TreeA.nearestNeighbor(q_rand);
        q_near = TreeA.getNodeState(nearId);
        %[nearId, q_near] = nearest_node(TreeA, q_rand);

        %Find the distance between the nearest neighbor of X, and X
        %distX_nn = stateValidator.StateSpace.distance(q_rand, q_near);
        distX_nn = qdist(q_rand-q_near);

        %Assume that it is not possible to add a new node in the TREE
        qNewAId = -1;
        statusExtend = robotics.manip.internal.RRTUtils.ExtendFailed;

        q_dir = (q_rand-q_near);
        q_dir = q_dir/qdist(q_dir);
        
        q_s = q_near + q_dir*eps;
        
        %if(isinf(eps) || distX_nn < eps)
        %    q_s = q_rand;
        %else
        %    q_s = stateValidator.StateSpace.interpolate(...
        %        q_near, q_rand, eps/distX_nn);
        %end
        
        %Contrain (added myself)
        %[err, lv_O] = task_error(q_s);
        
        q_r = q_s;
        
        [constrain_success, q_s] = constrain_FOR(q_r, q_near);
        
        if ~constrain_success
            continue;
        end
                
        % Find new closest (added myself)
        nearId = TreeA.nearestNeighbor(q_s);
        q_near = TreeA.getNodeState(nearId);
        %[nearId, q_near] = nearest_node(TreeA, q_s);
        if constrain_success && qdist(q_s-q_near) > eps*1.5
            constrain_success = false;
        end
        
        [err, lv_O] = task_error(q_s);
        
        % Check if belt collides with pulley
        constrain_success = constrain_success && check_belt_collision(lv_O, true);

        %Validate
        if(constrain_success && stateValidator.isStateValid(q_s))
            qNewAId = TreeA.insertNode(q_s, nearId);
            statusExtend = robotics.manip.internal.RRTUtils.ExtendSucceeded;
            total_its = total_its + 1;
        end
        
        %If the extend was successful, then extend TreeB towards
        %the newly added node in TreeA
        if(statusExtend == robotics.manip.internal.RRTUtils.ExtendSucceeded)

            %Assume that it is not possible to extend towards the newly
            %added node
            statusJoin = robotics.manip.internal.RRTUtils.ExtendFailed;

            joinId = TreeB.nearestNeighbor(q_s);
            q_join = TreeB.getNodeState(joinId);
            %[joinId, q_join] = nearest_node(TreeB, q_s);

            %new_dist = stateValidator.StateSpace.distance(q_s, q_join);
            new_dist = qdist(q_s-q_join);
            if new_dist < tree_dist
                tree_dist = new_dist;
                fprintf("%d = %f\n", k, tree_dist);
                %set_q(q_s, lv_O);
            end
            
            if new_dist < eps
                qNewBId = TreeB.insertNode(q_s, joinId);
                statusJoin = robotics.manip.internal.RRTUtils.ExtendSucceeded;
            end
                
            if(statusJoin == robotics.manip.internal.RRTUtils.ExtendSucceeded)
                path = ...
                    robotics.manip.internal.RRTUtils.joinTrees(...
                    TreeB, qNewBId, TreeA, qNewAId);
                
                path = path(:,1:6);
                
                if all(path(1,1:6) == goalState(1:6))
                    path = flipud(path);
                end
                %total_its

                %Populate the solution info when the path is found
                %solInfo.IsPathFound = true;
                %solInfo.ExitFlag = ...
                %    robotics.manip.internal.RRTUtils.GoalReached;
                return;
            end
        end
        [TreeA, TreeB] = robotics.manip.internal.RRTUtils.swap(TreeA, TreeB);
    end
    
    function [nId, nq] = nearest_node(tree, q_s)
        n = tree.getNumNodes;
        
        closeId = 0;
        close_dist = qdist(tree.getNodeState(closeId) - q_s);
        
        for Id = 1:n-1
            Iddist = qdist(tree.getNodeState(Id) - q_s);
            if Iddist < close_dist
                closeId = Id;
                close_dist = Iddist;
            end
        end
        
        nId = closeId;
        nq = tree.getNodeState(nId);
    end
end

function path = rrt(startState, goalState)
    global robot;
    %global collision_env;
    
    %rrt_obj = manipulatorRRT(robot, collision_env);
    rrt_obj = manipulatorRRT(robot, {});
    path = rrt_obj.plan(startState, goalState);
    path = path(:,1:6);
    
    fprintf("Found path of length %d\n", size(path,1));
end

function path_okay = check_path(path)
    global robot;
    global fingers_closed;
    global collision_env;
    environment = collision_env;
    
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
    eps = 0.02;
    path_okay = true;

    n = size(path,1);
    
    for i = 1:n-1
        
        q1 = path(i  ,:);
        q2 = path(i+1,:);
        
        dist = qdist(q2-q1);
        
        n2 = floor(dist/eps);
        
        for j = 1:n2
            node = q1+(j/(n2+1))*(q2-q1);
            
            [~, lv_O] = task_error(node);
            node_okay = stateValidator.isStateValid([node fingers_closed fingers_closed]) && check_belt_collision(lv_O, false);

            if ~node_okay
                path_okay = false;
                %return;
            end
        end
    end
end