%% Based on rasmusan.blog.aau.dk/files/ur5_kinematics.pdf
clc;
clear;
close all;

%% Setup
robot = importrobot('urdf/ur5_robot.urdf');
robot.show(robot.homeConfiguration);

%% Tests
for n = 1
    test_q = [1.8342 -0.4738 1.2711 0.0786 -1.1695 -0.4382];
    test_T = getTransform(robot, to_rtb_q(test_q, 0), 'ee_link', 'base_link');%*trotx(deg2rad(-90))*troty(deg2rad(90));
    
    qs = ur5_ik(test_T);
    
    best = 0;
    best_error = 999999999;
    for i = 1:size(qs,1)
        error = norm(qs(i,:)-test_q);
        if error<best_error
            best = i;
            best_error = error;
        end
    end
    
    best_q = qs(best,:);
    
    test_q
    best_q
    
    robot.show(to_rtb_q(test_q, 0));
    hold on;
    SE3(test_T).plot;
    robot.show(to_rtb_q(best_q, 0));
    hold off;
end

%% Vector q to RTB q (specific for the urdf file used)
function rtb_q = to_rtb_q(q, fingers)
    rtb_q = struct('JointName', {'shoulder_pan_joint' 'shoulder_lift_joint' 'elbow_joint' 'wrist_1_joint' 'wrist_2_joint' 'wrist_3_joint' 'gripper_finger_left_joint' 'gripper_finger_right_joint'}, 'JointPosition', {0 0 0 0 0 0 0 0});
    for i = 1:6
        rtb_q(i).JointPosition = q(i);
    end
    rtb_q(7).JointPosition = fingers;
    rtb_q(8).JointPosition = fingers;
end