clear all;
syms d r l pi alp;
a = 4*(2-pi)*d/(pi^2);
b = 4*(pi-2)*d/pi;
c = 2*pi*(r+d/2)-l;
ll = a*alp^2+b*alp+c;

alpha = (-b-sqrt(b^2-4*a*c))/(2*a)
s = simplify(alpha)-pi
s2 = 1/2*pi*(1-sqrt(d*(2*(pi-1)*d-l+2*pi*r)/(sqrt(pi-2)*d)))
s3 = 1/2*pi*(1+sqrt(d*(2*(pi-1)*d-l+2*pi*r)/(sqrt(pi-2)*d)))

min = subs(s,l,2*pi*r+2*d)
max = subs(s,l,2*pi*r+pi*d)

double(subs(subs(subs(min,d,2),r,0.1),pi,3.1415))
double(subs(subs(subs(max,d,2),r,0.1),pi,3.1415))

l = 1;
r = 0.1;
pi = 3.1415;
alp_of_d = subs(s)
%fplot(alp_of_d,[0 1])
d_min = double(subs((l-2*pi*r)/pi))
d_max = double(subs((l-2*pi*r)/2))
fplot(alp_of_d,[d_min d_max])

d = d_min;
double([subs(s); subs(s2); subs(s3)])

%alp = deg2rad(90);
double(subs(subs(ll,alp,deg2rad(90))))

double(2*d+2*pi*r)
double(2*pi*r+pi*d)