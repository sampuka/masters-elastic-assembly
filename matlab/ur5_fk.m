function T = ur5_fk(q)
%MY_FK Summary of this function goes here
%   Detailed explanation goes here
   
    %% UR5 DH Parameters
    alpha = [0 deg2rad(90) 0 0 deg2rad(90) deg2rad(-90)];
    a = [0 -0.425 -0.39225 0 0 0];
    d = [0.089159 0 0 0.10915 0.09465 0.0823];
    
    T = eye(4);
    for i = 1:6
        T = T * DHLink(alpha(i), a(i), d(i), q(i));
    end
end

%% Forward Kinematics DH
function T = DHLink(alp, a, d, ang)
    T = [cos(ang),          -sin(ang),         0,        a
         sin(ang)*cos(alp), cos(ang)*cos(alp), -sin(alp) -sin(alp)*d
         sin(ang)*sin(alp), cos(ang)*sin(alp), cos(alp), cos(alp)*d
         0,                 0,                 0,        1];
end