function rtde_path(path)
%UNTITLED Summary of this function goes here
%   Detailed explanation goes here
    p = path;
    
    N = size(p,1);
    
    p2 = zeros(N,9);
    
    for i = 1:N
        p2(i,1:6) = p(i,1:6);
        p2(i,7:9) = [0.6 0.3 0.0];
    end    
    
    arg_str = sprintf('[%f, %f, %f, %f, %f, %f, %f, %f, %f]', p2(1,:));
    for i = 2:N
        arg_str = strcat(arg_str, sprintf(', [%f, %f, %f, %f, %f, %f, %f, %f, %f]', p2(i,:)));
    end
    
    %rtde_c.moveL(position1);
    %rtde_c.moveL(position2);
    %rtde_c.stopRobot();
    raw_cmd = 'python3 -c ''import rtde_control; rtde_c = rtde_control.RTDEControlInterface("192.168.10.10"); rtde_c.moveJ([%s])'' ';
    
    cmd_string = sprintf(raw_cmd, arg_str);
    
    system(cmd_string)
end

