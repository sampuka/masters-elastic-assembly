classdef RRTNode
    %RRTNODE Summary of this class goes here
    %   Detailed explanation goes here
    
    properties
        q,
        root
    end
    
    methods
        function obj = RRTNode(q, root)
            %RRTNODE Construct an instance of this class
            %   Detailed explanation goes here
            if nargin > 0
                obj.q = q;
                obj.root = root;
            end
        end
    end
end

