clc;
clear;
close all;
%%

%mdl_ur5;
global robot;
robot = importrobot('urdf/ur10_urdf/ur10_robot.urdf');
cfg = robot.homeConfiguration;
%q = [-0.5+pi -0.7 1 1.47 1.571 0.5 0];
q = [0 -pi/2 pi/2 0 0 0];

fingers_open = 0;
fingers_closed = 0.024;

global belt_length;
belt_length = 0.4;
belt_radius = belt_length/(2*pi);

global belt_pnts;
belt_pnt_count = 50;
belt_pnts = zeros(3, belt_pnt_count);
update_belt_points(q, 0);

% EE -> Grasp TCP transformation
global T_ee_grasp;
T_ee_grasp = SE3(robot.getTransform(cfg, 'grasp_link', 'ee_link'));

% World -> Base transformation
global T_world_base;
T_world_base = SE3(robot.getTransform(cfg, 'base_link', 'world'));

%ur5.plot(q);
robot.show(to_rtb_q(q, fingers_closed));
hold on;
SE3().plot();
global belt_h;
belt_h = plot3(belt_pnts(1,:), belt_pnts(2,:), belt_pnts(3,:));

%pulley_radius = 0.02;
%pulleys = [-0.5 0.6 0; -0.57 0.63 0];
global pulleys;
% pulleys = [
%     -0.5880838282764489, -0.5671459938022354, 0.043017445308828936+0.1;
%     -0.7137812110005659, -0.5619578403410124, 0.041641062047986980+0.1
%     ];
% pulleys = [0.185, 0.11, 0.05+0.1;
%            0.11,  0.11, 0.05+0.1];
pulleys = [
    Pulley(transl(0.245, 0.11, 0.05+0.1), 0.03),
    Pulley(transl(0.110, 0.11, 0.05+0.1), 0.01)
    ];
    
%pulley_choice_temp = 1;

for i = 1:size(pulleys,1)
    [Xc,Yc,Zc] = cylinder(pulleys(i).radius);
    Zc = Zc*0.02;
    surf(Xc+pulleys(i).T.t(1), Yc+pulleys(i).T.t(2), Zc+pulleys(i).T.t(3)-0.02/2);
end

global collision_env
collision_env = {collisionCylinder(0.02, pulleys(1).radius), collisionCylinder(0.02, pulleys(2).radius)};
collision_env{1}.Pose = transl(pulleys(1).T.t);
collision_env{2}.Pose = transl(pulleys(2).T.t);
collision_env = {};

%% First waypoint
diff_T = inv(pulleys(1).T)*(pulleys(2).T);
%ang = atan2(pulleys(2,2)-pulleys(1,2), pulleys(2,1)-pulleys(1,1));
ang = atan2(diff_T.t(2),diff_T.t(1));

goal1_T = pulleys(1).T*SE3(troty(pi)*trotz(pi-ang)*transl([0 -pulleys(1).radius-0.015 0]));
goal1_T.plot
goal1_q = my_ik(goal1_T*inv(T_ee_grasp), q);

set_q(goal1_q, fingers_closed, 0);

%% Second waypoint
goal2_T = pulleys(2).T*SE3(troty(pi)*trotz(pi-ang)*transl([0 -pulleys(2).radius-0.015 0]));
goal2_T.plot
goal2_q = my_ik(goal2_T*inv(T_ee_grasp), goal1_q);

set_q(goal2_q, fingers_closed, 0);

%% Third Waypoint
goal3_T = pulleys(2).T*SE3(troty(pi)*trotz(-ang)*transl([0 -pulleys(2).radius-0.015 0]));
%goal3_T = goal3_T*SE3(transl([0.5 0 0 ]));
%goal3_T = goal3_T*SE3(trotx(0.5));
%goal3_T = goal3_T*SE3(troty(0.5));
goal3_T.plot
goal3_q = my_ik(goal3_T*inv(T_ee_grasp), goal2_q);

set_q(goal3_q, fingers_closed, 0);

%% TEsting
q_s = [goal2_q + (rand(1,6)-0.5)*0 0 0];
%q_s = [0.5014   -1.0406    1.6109   -2.1397   -1.5778   -1.0770         0         0];
Ft = pulleys(2).T;
while true
    C = [1 1 1 1 1 0];
    T_offset = inv(my_fk(goal2_q))*Ft;
    dT = inv(inv(my_fk(q_s))*Ft)*T_offset;
    dT = dT.double();
    psi = atan2(dT(3,2), dT(3,3));
    theta = atan2(-dT(3,1), norm([dT(3,2) dT(3,3)]));
    phi = atan2(dT(2,1), dT(1,1));
    err = C.*[dT(1:3,4)' psi theta phi];
    
    T_s = SE3(transl(my_fk(q_s).t));
    T_t = inv(Ft)*T_s
    ang_s = atan2(T_t.t(2),T_t.t(1))
    p = [cos(ang_s) sin(ang_s) 0] * norm(T_offset.t)
    err = [T_s.t'-p 0 0 0];
    
    J0_full = geometricJacobian(robot, to_rtb_q(q_s, 0), 'grasp_link');
    J0 = J0_full(:,1:6);
    R_0_t = inv(Ft.R);
    Jt = [R_0_t zeros(3); zeros(3,3) R_0_t]*J0;
    ctheta = cos(err(5));
    cphi = cos(err(6));
    stheta = sin(err(5));
    sphi = sin(err(6));
    E = [eye(3) zeros(3); zeros(3) [cphi/ctheta sphi/ctheta 0; -sphi cphi 0; cphi*stheta/ctheta sphi*stheta/ctheta 1]];
    J = E*Jt;
    dx = zeros(6,1);
    dx(1) = err(4);
    dx(2) = err(5);
    dx(3) = err(6);
    dx(4:6) = err(1:3);
    dx
    dq = (pinv(J)*(dx*0.1))';
    q_s = q_s - [dq 0 0];
    set_q(q_s, fingers_closed, 0);
    %pause(1);
end
error(' ');

%% Plan
path = my_rrt([q 0 0], [goal1_q 0 0], 0.10);
path2 = my_rrt([goal1_q 0 0], [goal2_q 0 0], 0.10);

Ftt = pulleys(2).T;
%Ftt = Ftt*SE3(rotm2tform(goal2_T.R));
Ftt.plot
path3 = my_rrt_constrained([goal2_q 0 0], [goal3_q 0 0], 0.01, [0 0 0 0 1 0], Ftt);

%% Shortcut
if true
    path_sc = shortcut(path);
    path2_sc = shortcut(path2);
else
    path_sc = path;
    path2_sc = path2;
end

%% Show path
for i = 1:size(path_sc,1)
    set_q(path_sc(i,1:6), fingers_closed, 0);
    pause(0.1);
end

%% Show path2
for i = 1:size(path2_sc,1)
    set_q(path2_sc(i,1:6), fingers_closed, 0);
    pause(0.1);
end

%% Show path3
for i = 1:size(path3,1)
    set_q(path3(i,1:6), fingers_closed, 0);
    pause(0.1);
end

%% RTB q to vector q (specific for the urdf file used)
function q = from_rtb_q(rtb_q)
    q = [0 0 0 0 0 0];
    for i = 1:6
        q(i) = rtb_q(i).JointPosition;
    end
end

%% Set q for robot
function set_q(q, fingers, phase)
    %global robot;
    %global T_ee_grasp;
    global robot;
    global belt_h;
    global belt_pnts;
    update_belt_points(q, phase);
    robot.show(to_rtb_q(q, fingers), 'PreservePlot', false, 'Frames', 'off');
    set(belt_h, 'XData', belt_pnts(1,:), 'YData', belt_pnts(2,:), 'ZData', belt_pnts(3,:)); 
    
    %T = my_fk(q);
    
    %SE3(T).plot;
    %SE3(T*T_ee_grasp).plot;
end

%% Forward Kinematics
function T = my_fk(q)
    global robot;
    %global T_ee_grasp;
    %T = SE3(trotz(pi)*ur5_fk(q))*T_ee_grasp;
    T = SE3(getTransform(robot, to_rtb_q(q, 0), 'grasp_link', 'world'));%*trotx(deg2rad(-90))*troty(deg2rad(90)));
    % I think the URDF file has a wrongly rotated end effector, so this is needed
end

%% Inverse Kinematics
function q = my_ik(T, q0)
    %ik = robotics.InverseKinematics('RigidBodyTree', robot);
    %weights = [1 1 1 1 1 1];
    %q = from_rtb_q(ik('ee_link', T.T, weights, to_rtb_q(q0)));
    
    global robot;
    global T_world_base;
    
    T = inv(T_world_base)*T;
    
    qs = ur10e_ik(T.double);
    qd = zeros(8,1);
    for i = 1:size(qd,1)
        qd(i) = norm(wrapToPi(qs(i,:) - q0));
    end
    
    best_dist = qd(1);
    best = 1;
    
    for i = 2:size(qd,1)
        dist = qd(i,:);
        if dist < best_dist
            best = i;
            best_dist = dist;
        end
    end
    
    q = qs(best,:);
    q0
    qs
    qd
    best
    q
end

function alpha = belt_angle(d)
    l = 1.5;
    r = 0.03;
    alpha = 1.5708 - (1.4708*(-d*(2*d - l + 6.283*r))^(1/2))/d;
end

function update_belt_points(q, phase)
    global belt_pnts;
    global belt_length;
    global pulley_radius;
    pnt_count = size(belt_pnts,2);
    if phase == 0 % Just robot grabbing
        lerped = linspace(0,2*pi, pnt_count);
        r = belt_length/(2*pi);
        T = my_fk(q)*SE3(transl([0 r 0]));
        for i = 1:pnt_count
            belt_pnts(1:3,i) = T.t + T.R*[cos(lerped(i))*r; sin(lerped(i))*r; 0];
        end
    end
    
    if phase == 1
        r = belt_length/(2*pi);
        T = my_fk(q)*SE3(transl([0 r 0]));
        d = norm(pulley(1,1:3)-T.t);
        ang = belt_angle(d);
        rat = 4*ang/belt_length; % ratio of length clammed to pulley to total belt length
        for i = 1:rat*pnt_count
            lerped = linspace(0,2*ang, rat*pnt_count);
            belt_pnts(1:3,i) = pulley(1,1:3)+[cos(lerped(i)) sin(lerped(i)) 0]*pulley_radius;
        end
        for i = 1:pnt_count/2-2*rat*pnt_count
            lerped = linspace(ang,pi-ang, pnt_count/2-2*rat*pnt_count);
            belt_pnts(1:3,rat*pnt_count+i) = pulley(1,1:3)+[cos(lerped(i)) sin(lerped(i)) 0]*pulley_radius;
        end
    end
end

function length = path_length(path)
    length = 0;
    for i = 1:size(path,1)-1
        length = length + norm(path(i+1,:)-path(i,:));
    end
end

function path = my_rrt(startState, goalState, maxConnectionDistance)
    path = my_rrt_constrained(startState, goalState, maxConnectionDistance, [0 0 0 0 0 0], SE3());
end

function path = my_rrt_constrained(startState, goalState, maxConnectionDistance, C, Ft)
    function err = task_error(qs)
%         function task_dx = task_error_dx(qq)
%             Fe = my_fk(qq);
%             Tet = inv(Fe)*Ft;
%             %Tet.plot;
%             Tet = Tet.double();
%             phi = atan2(Tet(2,1),Tet(1,1));
%             theta = atan2(-Tet(3,1), norm([Tet(3,2) Tet(3,3)]));
%             psi = atan2(Tet(3,2), Tet(3,3));
% 
%             task_dx = C.*[Tet(1:3,4); phi; theta; psi]';
%             %Tet;
%         end
        
       %dx1 = task_error_dx(qs);
       %dx2 = task_error_dx(qnear);
       dT = inv(inv(my_fk(qs))*Ft)*T_offset;
       dT = dT.double();
       psi = atan2(dT(3,2), dT(3,3));
       theta = atan2(-dT(3,1), norm([dT(3,2) dT(3,3)]));
       phi = atan2(dT(2,1), dT(1,1));
       err = C.*[dT(1:3,4)' psi theta phi];
%        for p = 3:6
%            if dx(p) > pi/2
%                dx(p) = dx(p) - pi;
%            elseif dx(p) < -pi/2
%                dx(p) = dx(p) + pi;
%            end
%        end

%        err = 0;
%        for ii = 1:6
%            err = err + abs(dx(ii));
%        end
    end
    
    startState(7:8) = [0 0];
    goalState(7:8) = [0 0];
    should_constrain = any(C);
    
    T_offset = inv(my_fk(startState))*Ft;
    
    c_i_fails = 0;
    c_j_fails = 0;
    
    tree_dist = norm(goalState-startState);
    
    %Ft
    %Ft.plot
    if should_constrain && norm(task_error(goalState)) > 0.01
        error("Task error between start and goal configuration! Error = %d", norm(task_error(goalState)));
    end
    
    total_its = 0;
    
    global robot;
    global collision_env;
    global pulleys;
    environment = collision_env;
    maxNumIterations = 1000000;
    max_error = 0.0001;
    %maxConnectionDistance = 0.05;
    enableConnectHeuristic = false;
    path = [];
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
    
    maxNumNodes = maxNumIterations + 1;
    TreeA = nav.algs.internal.SearchTree(startState, maxNumNodes);
    TreeB = nav.algs.internal.SearchTree(goalState, maxNumNodes);
    TreeA.setCustomizedStateSpace(stateValidator.StateSpace);
    TreeB.setCustomizedStateSpace(stateValidator.StateSpace);
    
    randState =...
        stateValidator.StateSpace.sampleUniform(maxNumIterations);
    q_limits = zeros(6,2);
    for i = 1:6
        q_limits(i,1) = min([startState(i) goalState(i)])-0.2;
        q_limits(i,2) = max([startState(i) goalState(i)])+0.2;
    end
    
    for k = 1 : maxNumIterations
        q_rand = randState(k, :);
        q_rand(7:8) = [0 0];
        
        for i = 1:3
            q_rand(i) = rand()*(q_limits(i,2)-q_limits(i,1))+q_limits(i,1);
        end
        
        % 1: -2 1
        % 2: -pi/2 0
        % 3: 0 pi
        %q_rand(1) = rand()*(1-(-2))+(-2);
        %q_rand(2) = rand()*(0-(-pi/2))+(-pi/2);
        %q_rand(3) = rand()*(pi-(0))+(0);

        %Extend the TreeA towards a random configuration
        %[statusExtend, qNewA, qNewAId] =...
        %    robotics.manip.internal.RRTUtils.extend(...
        %    stateValidator, TreeA, qRand, maxConnectionDistance);
        nearId = TreeA.nearestNeighbor(q_rand);
        q_near = TreeA.getNodeState(nearId);

        %Find the distance between the nearest neighbor of X, and X
        distX_nn = stateValidator.StateSpace.distance(q_rand, q_near);

        %Assume that it is not possible to add a new node in the TREE
        qNewAId = -1;
        statusExtend = robotics.manip.internal.RRTUtils.ExtendFailed;

        if(isinf(maxConnectionDistance) || distX_nn < maxConnectionDistance)
            q_s = q_rand;
        else
            q_s = stateValidator.StateSpace.interpolate(...
                q_near, q_rand, maxConnectionDistance/distX_nn);
        end
        
        %Contrain (added myself)
        c_i = 0;
        c_j = 0;
        %err = task_error(q_s, q_near);
        if should_constrain
            err = task_error(q_s);
        else
            err = zeros(1,6);
        end
        
        pre_err = norm(err);
        
        constrain_success = norm(err) < max_error;
        if should_constrain && constrain_success
            disp('!');
        end
        q_r = q_s;
        
        while ~constrain_success % First Order Refraction
            J0_full = geometricJacobian(robot, to_rtb_q(q_s, 0), 'grasp_link');
            %J0 = zeros(6);
            %J0(:,1:3) = J0_full(:,4:6);
            %J0(:,4:6) = J0_full(:,1:3);
            J0 = J0_full(:,1:6);
            R_0_t = inv(Ft.R);
            Jt = [R_0_t zeros(3); zeros(3,3) R_0_t]*J0;
            ctheta = cos(err(5));
            cphi = cos(err(6));
            stheta = sin(err(5));
            sphi = sin(err(6));
            E = [eye(3) zeros(3); zeros(3) [cphi/ctheta sphi/ctheta 0; -sphi cphi 0; cphi*stheta/ctheta sphi*stheta/ctheta 1]];
            J = E*Jt;
            dx = zeros(6,1);
            dx(1:3) = err(4:6);
            dx(4:6) = err(1:3);
            dq = (pinv(J)*(dx*0.1))';
            q_s = q_s - [dq 0 0];
            
            if norm(q_s-q_r) > norm(q_r-q_near)
                break;
            end
            
            err = task_error(q_s);
            
            if norm(err) < max_error
                constrain_success = true;
            end
        end
        
        if ~constrain_success
            continue;
        end
        
%         while ~constrain_success % Random Gradiant Descent
%             if c_i > 2000
%                 c_i_fails = c_i_fails + 1;
%                 break;
%             end
%             if c_j > 30
%                 c_j_fails = c_j_fails + 1;
%                 break;
%             end
%             c_i = c_i + 1;
%             c_j = c_j + 1;
%             
%             q_s2 = q_s + [(rand(1, 6)-0.5)*2*err/200 0 0];
%             %err2 = task_error(q_s2);
%             err2 = task_error(q_s2);
%             
%             if norm(err2) < norm(err)
%                 q_s = q_s2;
%                 err = err2;
%                 c_j = 0;
%             end
%             
%             if norm(err) < max_error
%                 constrain_success = true;
%             end
%         end
        
        % Make sure it goes the correct way around last pulley
        if should_constrain
            t_s = my_fk(q_s).t';
            if norm(t_s-pulleys(1).T.t) < norm(pulleys(2).T.t-pulleys(1).T.t)
                constrain_success = false;
            end
        end
        
        % Find new closest (added myself)
        nearId = TreeA.nearestNeighbor(q_s);
        q_near = TreeA.getNodeState(nearId);
        if constrain_success && stateValidator.StateSpace.distance(q_s, q_near) > maxConnectionDistance + 0.001
            constrain_success = false;
        end

        %Validate
        if(constrain_success && (true || stateValidator.isMotionValid(q_near, q_s)))
            qNewAId = TreeA.insertNode(q_s, nearId);
            statusExtend = robotics.manip.internal.RRTUtils.ExtendSucceeded;
            if false && should_constrain
                pre_err
                post_err = task_error(q_s, startState)
            end
            if false %rrt debug
                %set_q(q_rand, 0, 0);
                %pause(1);
                %set_q(q_near, 0, 0);
                %pause(1);
                set_q(q_s, 0, 0);
                pause(0.5);
            end
            total_its = total_its + 1;
        end

        if false && mod(k,1000) == 0
            disp(k);
            disp(q_rand);
            disp(TreeA);
        end
        
        %If the extend was successful, then extend TreeB towards
        %the newly added node in TreeA
        if(statusExtend == robotics.manip.internal.RRTUtils.ExtendSucceeded)

            %Assume that it is not possible to extend towards the newly
            %added node
            statusJoin = robotics.manip.internal.RRTUtils.ExtendFailed;

            %If connect heuristic is enabled, then attempt to join the
            %trees all the way, else extend by maxConnectionDistance
            if(enableConnectHeuristic)
                [statusJoin, qNewBId] =...
                    robotics.manip.internal.RRTUtils.connect(...
                    stateValidator, TreeB, q_s);
            else
%                 [~, qNewB, qNewBId] = ...
%                     robotics.manip.internal.RRTUtils.extend(...
%                     stateValidator, TreeB, q_s, maxConnectionDistance);
% 
%                 %If the newly added node is the same as the node the
%                 %tree wants to extend to, then the trees can be joined
%                 if(isequaln(qNewB, q_s) && (qNewBId ~= -1))
%                     statusJoin = robotics.manip.internal.RRTUtils.ExtendSucceeded;
%                 end

                joinId = TreeB.nearestNeighbor(q_s);
                q_join = TreeB.getNodeState(joinId);
                 
                new_dist = stateValidator.StateSpace.distance(q_s, q_join);
                if new_dist < tree_dist
                    tree_dist = new_dist;
                    fprintf("%d = %f\n", k, tree_dist);
                end
                if new_dist < maxConnectionDistance
                    qNewBId = TreeB.insertNode(q_s, joinId);
                    statusJoin = robotics.manip.internal.RRTUtils.ExtendSucceeded;
                end
                
            end
            if(statusJoin == robotics.manip.internal.RRTUtils.ExtendSucceeded)
                if(mod(k, 2) == 0)
                    path = ...
                        robotics.manip.internal.RRTUtils.joinTrees(...
                        TreeB, qNewBId, TreeA, qNewAId);
                else
                    path = ...
                        robotics.manip.internal.RRTUtils.joinTrees(...
                        TreeA, qNewAId, TreeB, qNewBId);
                end

                path = path(:,1:6);
                c_i_fails
                c_j_fails
                total_its
                %TreeA.getNumNodes()
                %TreeB.getNumNodes()

                %Populate the solution info when the path is found
                %solInfo.IsPathFound = true;
                %solInfo.ExitFlag = ...
                %    robotics.manip.internal.RRTUtils.GoalReached;
                return;
            end
        end
        [TreeA, TreeB] = robotics.manip.internal.RRTUtils.swap(TreeA, TreeB);
    end
end

function new_path = shortcut(path)
    global robot;
    global collision_env;
    environment = collision_env;
    eps = 0.05;
    ss = robotics.manip.internal.ManipulatorStateSpace(robot);
    stateValidator = ...
                robotics.manip.internal.ManipulatorStateValidator(...
                    ss, environment, 0.01);
                
    new_path = path;
    
    length = path_length(new_path);
    start_length = length;
    fprintf("Path starts at length %f\n", start_length);
    
    max_iterations = 2000;
    for i = 1:max_iterations
        n = size(new_path,1);
        a = 0;
        b = 0;
        while b < a+2
            a = randi([1 n-2]);
            b = randi([3 n]);
        end
        
        q1 = new_path(a,:);
        q2 = new_path(b,:);
        
        qdist = norm(q2-q1);
        
        n2 = 2+floor(qdist/eps);
        
        lerp_path = zeros(n2, 6);
        
%        if n2 == 1
%            lerp_path(1,:) = q1+(q2-q1)/2;
%        else
        for j = 1:n2
            lerp_path(j,:) = (q1*(n2-j)+q2*(j-1))/(n2-1);
        end
%        end
        
        shortcut_valid = true;
        
        for j = 1:n2
            if false && ~(stateValidator.isStateValid([lerp_path(j,:) 0 0]))
                shortcut_valid = false;
                break;
            end
        end
        
        if shortcut_valid
            new_path = [new_path(1:a-1,:); lerp_path; new_path(b+1:n,:)];
            new_length = path_length(new_path);
            if new_length < length
                length = new_length;
                fprintf("%d = %f (%f%%)\n", i, length, (length-start_length)/start_length);
            end
        end
    end
end