## Time schedule for Master Thesis

September and October
    Research elastic model.
    Research state of the art for motion planning for elastic objects.
    Etc.

    Write down what I have learned.
    Choose a model and approach to continue work with.

November and December
    Develop chosen approach.
    Start with the elastic model.
    Create some kind of simulation to ensure correctness (find physical properties of actual belt?).
    Create some kind of simulation workspace for developing motion planning.
    Describe in report for anything that works.

2021
    (probably revise this time schedule)
    Possibly post-pone use of real robot until more simulation work has been done.
    The more I prepare before using a real robot, the better, I think
    When I get something working on the real robot, I think I will have a choice between how to continue;
        - attempt to generalize problem?
        - look into using dual robot?
        - use different approaches, compare to each other?
    Very difficult to predict from here on, I think.
    I would like to have decent time for writing up the report.
