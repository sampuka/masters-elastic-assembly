\documentclass[../Main.tex]{subfiles}

\section{Discussion}
The test results of the setup with three pulleys is
as can be expected. The successrate of the path planner is high throughout all
values of $\epsilon$ and the path is approved by the path check except for that
of the high $\epsilon$. The average planning time seems highly reliant of the
$\epsilon$.

However, the test with only two pulleys shows unexpected results. The
average planning time is high and the successrate by iteration is low,
regardless of $\epsilon$. The performance is mostly worse than the equivalent
in the setup with three pulleys.
There are several possibilities, among those are issues with the implementation and
conduction of the test. This can include software bugs and all the complexities
of the environment of the PC that ran the tests. Among others, this can be
varying CPU load at different points in the test due to other software.

These factors are hard to control, and since the issue seems general for the
setup with two pulleys, it is most likely something to do with the setup.
One of the issues that a sampling-based path planning algorithm can face,
is that of "narrow passages". A narrow passage is an area in configuration
space which the planner must go through, in which there is very little room to
avoid collisions. In this case with the belt and pulley system, there is
expected to be a narrow passage as the gripper needs to go around the last
pulley. Around this pulley, the belt is likely stretched very tight and does
not allow the gripper to pull it further than just necessary. Figure
\ref{fig:narrow-passage} illustrates a narrow passage in configurations space,
where white is collision-free region, and black those that are in collision.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.4\textwidth}{
    \includegraphics[width=0.4\textwidth]{figures/narrow-passage.png}
       }}
    \caption{An example configuration space illustrating a narrow passage. Black indicate states in collision and white collision-free.}
    \label{fig:narrow-passage}
\end{figure}

The setup with three pulleys is expected to encounter this problem to a lesser
degree, as the angle that the gripper needs to clear around the last pulley is lower,
and that the belt is in general longer, implying that it can be stretched further.
Therefore the theoretical narrow passage is shorter and wider.
If the main obstacle for the planner is the narrow passage, it would makes see
to see a weaker relationship between $\epsilon$ and planning time, as no matter
which extension distance is used, the chance of sampling a configuration
inside the narrow passage is similar.

The narrow passage problem is known in the field of robotics, and there are ways
to modify planners to handle the issue. Zhong et al. \cite{zhong:narrow-passage}
modifies a Probabilistic Roadmap (PRM) planner to identify areas recognized as
narrow passages and to build nodes in the roadmap before the usual planner
is invoked. The PRM algorithm is similar to RRT in that they are
sampling-based algorithms, but differ in the data structure used to store
the nodes. The modified PRM find configurations in which nearby configurations
show certain patters in whether they are inside or outside collision objects.
This differs slightly from the situation in the project, as there are two
ways a sampled configuration can be invalid, either by a collision object or
by the restrictions imposed by the belt. However, it is easy to detect the
second case by whether the value of the \texttt{TaskSpaceError} is above
the accepted $err_{max} = 0.001$. While the data structure of the RRT
algorithm does not support the equivalent of adding nodes to the roadmap in
narrow passages, all of the modifications to the RRT algorithm in this project
could be done to a PRM planner instead. This would indicate that a PRM
planner is more suited to handle the belt and pulley problem, as it can
mitigate the narrow passage problem this way.

Worth noting is that when the gripper moves around a pulley, one side of the
narrow passage is a collision with the pulley, but the other side are states
in which the belt constraints create a task space error because the
belt is pulled too tight. The path planning does
not discard these configurations, but instead attempts to correct them using
first-order retraction. There is a possibility that this correction is able to
move the states from this side of the narrow passage into it. The performance
test, especially that with only two pulleys does not support this hypothesis.
One possibility is that the way this task space error is found tends to
overshoot. The method of inserting a triangle to find the magnitude of the
correction and the way of finding the angle of this correction, as illustrated in
Figure \ref{fig:triangle} may not be ideal.

The test shows that the path optimization algorithm greatly improves
the length of the path found by the planner, but does not show an ability to
consistently lower the length to an optimal length, as indicated by the high
standard deviation for the path length in the performance test. This can be
because the iteration limit for the optimization algorithm should be increased,
but there may also be a problem regarding the angle at which the robot
attaches the belt. Figure \ref{fig:high-low-angle} shows two different angles
at which the path planner might find an acceptable path.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/high-low-angle.jpg}
       }}
    \caption{Two pictures illustrating a high and low angle for clearing a pulley.}
    \label{fig:high-low-angle}
\end{figure}

The high angle is better
in regards to configuration space path length, as the start and goal configurations
of the path planner is with a high angle. However, due to random chance, the
planner may find a path with a lower angle. The shortcut path pruning algorithm
is not apt to optimize in regards to this angle, however the partial shortcut
algorithm as proposed by Garaerts et al. \cite{geraerts:partial-shortcut} may be able to do so
efficiently. The partial shortcut algorithm is similar, but does only
attempt to shortcut between two nodes in specific degrees of freedom.
In the partial shortcut algorithm, the orientation can be considered a single degree
of freedom, and would therefore be possible to use for this problem.

The overall test shows that the implementation supports moving and rotating
the white board with the pulleys. This indicates that the method is successfully
defined as to handle the general case of the belt and pulley system, no matter
the specifics of the placement. This is one of the reasons for defining and
using the task frame.

All tests feature a low amount of repeats, which weakens any conclusions based
on the results. Ideally, the tests would be reperformed with a higher number
of iterations. The variety of setups is low, and does not feature any setups
using many pulleys, environments where the robot needs to avoid collisions or
any setups that uses larger distances between pulleys.

The first part of the task, that which attaches the belt to the first pulley
and pulls the belt taut, is simply assumed to work. The reliability of this
part would ideally be tested for, even though it has been observed to be
almost completely reliable throughout development.

The majority of CPU time spent during planning is spent on collision detection
between the robot, gripper and collision objects in the environment.
The use of lower quality models for collision checking does not yield
significant increase in efficienty. It is thought that the collision
detection built into MATLAB's Robotics Systems Toolbox isn't developed with
high performance as a priority.

There are some ways in which the method may be improved. The optimal path for
most setups is similar for any $\epsilon$ value. Potentially, it is possible to
find a path using a high $\epsilon$, and use this rough path as a help for
finding a path using a lower $\epsilon$ value. For system that have pulleys
further apart, the distance to path can be much greater. In such systems it may
be necessary to dynamically change $\epsilon$, so that it is high far from
pulleys and low when it needs to clear a pulley.

The validation path check uses the same taut belt assumption as the planning
does. Since the performance of the path check is not important, it is
possible to use a more accurate belt model. This would allow a more accurate
path check and would also give insights into what forces the belt is
subject to.
