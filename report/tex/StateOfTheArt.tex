\documentclass[../Main.tex]{subfiles}

\section{State of the Art}
This section will introduce research on methods and techniques relevant to the problem
statement.

\subsection{Elastic Model}
The belt is apt to be modeled as a looped cable. Modeling of cables is
useful in many fields, so plenty research on modeling cables has been done,
and many models have been developed, as can be seen in Lv et al. \cite{Lv:review}.

In their review of models, Lv et al. make a distinction between dynamic
and quasi-static models. Among the dynamic models are the mass-spring model
and the multi-body model, and among the quasi-static models are the elastic rod
model, the dynamic spline model and the finite element model.
Compared to the dynamic models, the quasi-static models share improved
theoretical background, greater accuracy, higher calculation time, and
increased complexity and implementation difficulty. While comparitively less accurate, the
dynamic models can be very good. For this reason and the much better calculation
time, the quasi-static models will not be used or elaborated on.

In their review of modeling flexible deformable objects,
Hou et al. \cite{Hou:review} present the finite element method and the mass-spring model.

\subsubsection{Mass-Spring Model}
The mass-spring model is a simple geometric model that approximates the state of a
cable as a series of joints. Each joint has a mass and three-dimensional
position. Each joint is connected to the last and next joint in the cable with
a link, represented by a spring. The sum of the springs' rest lengths represents the length of the cable, and
each spring itself represents the strechability of the cable. Constraints
can be imposed on the ends of the cable to represent it being held in place.
Figure \ref{fig:mass-spring} illustrates a mass-spring model.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/mass-spring.png}
       }}
    \caption{The mass-spring model made of joints connected with linear springs.}
    \label{fig:mass-spring}
\end{figure}

This description can approximate a cable (and soft bodies in general) in some situations,
but its simplicity can make it very inaccurate, as it only enforces the length
of the cable, and not its shape. The model has been expanded to include bending forces,
by introducing a rotational spring at each joint that attempts to minimize the
angle between the joint's two links.

The model has been further expanded to include twisting to further the accuracy,
as described by Lv et al. \cite{lv:wires}.
As a joint does not encode a rotation, the twisting forces for a joint is based
on two planes. The first plane is defined by the position of the joint
and the previous two joints, and the second plane is defined by the joint, the
previous joint and the next joint. Lv et al. show that this expansion of the model
can give accurate results.

\subsubsection{Multi-Body Model}
The multi-body model is a simple geometric model which approximates a cable
using a chain of bodies, connected end to end. Each body represents a segment
of the cable and encodes a position and rotation. Figure \ref{fig:multi-body}
illustrates the multi-body model.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/multi-body.png}
       }}
    \caption{The multi-body model made of links connected end-to-end.}
    \label{fig:multi-body}
\end{figure}

The model is often extended to represent stretching by including linear springs
between bodies, and bending by adding angular springs. Servin et al. \cite{4407699} describes
a set of constraints for the dynamics, adding a twisting force to increase accuracy.

\subsubsection{Finite Element Method}
The finite element method is method for discretizing an object into a mesh.
The method subdivides the object into smaller parts, by descretizing the space
dimensions. Formulating a boundary value problem for results in a set of
algebraic equations for the smaller parts. These equations are used to
formulate a set of differential equations for the entire object, which can
be solved by minimizing an error function \cite{bathe:fem}.
While the method is widely used for
many physical problems such as structural analysis, the method is extensive,
and is not suited to this project's use case, a one-dimensional cable.

\subsubsection{Energy Model}
Using an optimization-based motion planning, Ramirez-Alpizar et al. \cite{RamirezAlpizar2018ASA}
described the state of the
belt purely by its strectching energy, as found by the constraints imposed by
the two robots holding it. The paper has a focus on not over-stretching the
belt, and simply does not consider the position of the belt.
It was able to find a trajectory, however the test and trajectory were simple,
and it may not work for more complex use cases.

\subsubsection{Infinite Representation Models}
It is possible to model some elastic objects with geometric shapes such that
it has a finite amount of parameters, but has infite representation. This can include
circles, ellipses and straight lines. Using such a model introduces only few
parameters to the state of the system, and can be computationally efficient.
However, few things can be accurately represented using simple shapes, and
may need certain assumptions to be able to approximate an object.

\subsubsection{Applying Model to a Looped Belt}
For applying a linear cable model to a looped belt, it is possible to
constrain the two end points to each other. In the case of mass-spring model,
apply the proper linear, angular and twisting springs between the two ends.
The place that is being held by the robot manipulator can be represented by
constraining two or more points in the cable. This stops both movement and
rotations.

The bending force for a typical cable attempts to keep the cable straight.
In the case of a looped belt, its natural state is bent, and so the calculation
of the bending force must consider a non-zero rest angle.

\subsection{Motion Planning}
Motion planning methods vary greatly. Some methods such as Rapidly-exploring Random Tree (RRT) and Probabilistic Roadmap (PRM)
are kinematic in nature, whereas some methods such as kinodynamic planning are
can include dynamic and therefore physical movement in its planning. Further, there is a distinction between stochastic methods and
optimization-based methods. Optimization-based methods attribute the state with
a cost function, which is minimized, whereas stochastic methods rely on changing
the state randomly, often being sampling-based.

Methods that are kinematic do not include a time element. Typically after the
path has been found, will timesteps between them be added, based on desired
velocities and accelerations. However it is during planning that the time-dependent
dynamics of the elastic object is relevant. It is possible to discard the dynamics
of the elastic object imposed by movement by defining the state of the
elastic object as its rest state given the constraints it is imposed by the robot.
Some methods are able to fully integrate the state and dynamics of the
elastic object. However often at the expense of simplicity and planning speed.

\subsubsection{Taut Manipulation}
Miura and Ikeuchi\cite{Miura:taut} proposes a method of inserting the belt around the pulleys of
a belt drive using 'fingers' to keep the belt taut at all times.
With this constraint, the state of the belt is uniquely determined at all times
and a dynamic elastic model is not needed. They define the state as the set of these fingers
and define a set of transitions these fingers can make in order to change the state. This approach
does not use a grasping robot manipulator and requires several fingers.

\subsubsection{Elastic Object Planning}
Lamiraux and Kavraki\cite{Lamiraux:elastic-planning} proposes an approach to motion planning of an elastic
object in isolation. They use two robot manipulators grapsing the ends of
an elastic cable-like object, attempting to reach a goal state inside the
working environment. The necessary movement of the manipulators are not
considered. They use several different elastic models including mass-spring,
and finds its position using energy minimization methods. The planning
algorithm is a typical roadmap approach.

Planning only on the elastic object assumes that there is a valid state
for the robot manipulators for any state of the elastic object. Therefore
the found path for the elastic object may be impossible to perform due to environment
collisions.
