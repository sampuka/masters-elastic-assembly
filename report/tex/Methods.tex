\documentclass[../Main.tex]{subfiles}

\section{Methods}
This section formulates the task to be solved, and explains the approach to solving it.
A path planning and path optimization algorithm is modified to satisfy the
constraints imposed by the chosen approach. The section will also detail some
of the implementation specifics.

This section features pseudocode. Methods which are of interest and an
integral part of the solution are written using a \texttt{monospace font}. Miscellaneous
methods which are intended to be self-explanatory are written using $italic$ $font$.
Most pseudocode is simplfied as to avoid cluttering the essense of the algorithm with
things only important to the implementation, such as error handling.

For any transformation matrix $T_i$, the translational component is denoted
$T_{i,t}$, the rotational component is denoted $T_{i,R}$ and the three
cartesian components of the translational component is denoted $T_{i,x}$, $T_{i,y}$ and $T_{i,z}$.

\subsection{Pulley and Belt Problem}
This section will define the details of the pulley and belt system.
Figure \ref{fig:belt-pulley-example} illustrates such a system, with the belt attached.

The pulleys are defined by a transformation describing their position and
orientation, their radius and their height. The pulleys can freely rotate and are symmetric
around the rotation axis. The orientation is defined such that the z axis is
the axis about which this rotation happens.
Here, it is assumed that the pulleys are oriented such that their xy planes
are the same. This means that no pulleys are 'higher' than the others, and if a
belt is attached around all pulleys, the belt would lie in a single plane.

\textbf{Assumption 1: All pulleys are aligned on a common plane.}

The pulleys have a transformation from origin $T_i$, radius $r_i$ and height $h_i$,
where $i \in \mathopen[1,n\mathopen]$ and $n$ is the amount of pulleys.
These are illustrated in Figure \ref{fig:scene-with-pulleys}. Note that these
transformations do not need to have a specific rotations around the z axis.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/scene-with-pulleys.png}
       }}
    \caption{A belt and pulley system illustrating $T_i$, $r_i$ and $h_i$.}
    \label{fig:scene-with-pulleys}
\end{figure}

The order in which the pulleys are given defines the order in which the belt will
be attached, starting with pulley $1$. The method is developed such that
there may be any number of pulleys, but at least $2$. The natural length of
the belt is denoted $l_0$.

A task frame $F_t$ with the transformation of $T_1$, rotated about the z axis
such that the y axis points
towards $T_2$ is defined, and makes the basis for the 'task space'. Figure
\ref{fig:task-frame} shows a belt and pulley system with the task
frame is illustrated.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/task-frame.png}
       }}
    \caption{A belt and pulley system with the task frame $F_t$ visualized.}
    \label{fig:task-frame}
\end{figure}

This task space will be used to simplify some of the notation in the
path planning, and allows the pulleys to be uniformly translated or rotated
without impacting any analyses made in the task space. As all pulleys
lie in the xy plane of the task space, the xy plane is a useful illustration.
Figure \ref{fig:task-space-example} shows a scene with three pulleys in this plane.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/task-space-example.jpg}
       }}
    \caption{Example task space with three pulleys.}
    \label{fig:task-space-example}
\end{figure}

From the perspective of xy plane of the task space, the belt will be attached
clockwise. This
assumption limits the scope of the problem, and greatly simplifies the
implementation. However, some paths may only be possible in either clockwise or
anti-clockwise due to collision objects in the environment, so the assumption
limits which setups it is possible to create a path for.

\textbf{Assumption 2: The belt will be attached clockwise in task space.}

Some belt and pulley systems are designed to have the belt touch the insides
of some pulleys, making a concave shape. For this project, only convex shapes,
where the belt is attatched to the outside of all pulleys, is considered. The
proposed method for solving the task is not suited to handle this type of
problem.

\textbf{Assumption 3: The belt is attached to the outside of all pulleys.}

Figure \ref{fig:solution-example} shows steps from an example solution to the task, where the
rectangle and the lines represents the gripper and the belt segments. The gripper is
illustrated as belonging to the xy plane, even though this may not be the case.
To aid the reader, a video of the robot attaching the belt using the
method proposed in this project can be found in the Digital Appendix or on
\href{https:/www.youtube.com/watch?v=cxrMcDy_bbc}{YouTube}
\footnote{\url{https:/www.youtube.com/watch?v=cxrMcDy_bbc}}.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.7\textwidth}{
    \includegraphics[width=0.7\textwidth]{figures/solution-example.jpg}
       }}
    \caption{Example solution with three pulleys.}
    \label{fig:solution-example}
\end{figure}

\subsubsection{Approach}
The initial state in the problem is the robot freely grasping the belt at
one place in some home configuration. The goal is for the belt to be attached around all pulleys.

The task is divided into two main parts. The first is to attach the belt to
the first pulley and pull the belt taut.
The second part is to plan a path for the robot such that it
attaches the belt to all pulleys. This task can be simplified due to
the first part, as the belt can be assumed taut. This assumption is used to
approximate the state of the belt using straight lines between points on the
pulleys or on the gripper.

\textbf{Assumption 4: The state of the belt can be adequately approximated using straight lines, if the belt is pulled taut.}

To ensure the planned
movements correctly attaches, and the belt doesn't produce collision
problems, constraints are formulated which should be satisfied for all movement in the
second part.

As the belt is attached to the pulleys clockwise in task space, the part of the
belt which touches the 'left' side of the gripper must collide with the pulleys
such that is attaches. The part which touches the 'right' side will move above the pulleys to avoid
collision. The left side of the gripper is defined as $T_l$, and the right side
as $T_r$, and these two points can be seen in Figure \ref{fig:left-right-points}.
These points will often serve as one end of a line, representing a
taut belt segment. $T_l$ is constrained to move in the $z=0$ plane in task space, ensuring the
belt collides correctly and attaches to the pulleys.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.3\textwidth}{
    \includegraphics[width=0.3\textwidth]{figures/left-right-points.png}
       }}
    \caption{An illustration of the two sides of the gripper, $T_l$ and $T_r$.}
    \label{fig:left-right-points}
\end{figure}

\textbf{Assumption 5: Constraining $T_l$ to $z=0$ in task space ensures that the belt attaches to pulleys correctly.}

In any position, the
right-side belt segment may not collide with any pulley. Any state is tested for
collision between the line of the belt segment and all pulleys except the first,
which it always connects to.

The belt may not be stretched further than it can physically handle without
deforming or pulling back on the gripper with too high a force. This is
done by finding the length of the belt model in any state and comparing it
to the natural length of the physical belt. Neither may the belt be too loose in
any state as the belt might detach from a pulley, irreversibly violating
the assumption of a taut belt.

The allowed range for the length
of the belt is defined with the help of a $slack$ value. With a natural belt
length of $l_0$, the belt may not be below $l_0 \cdot (1-slack)$ and may not
exceed $l_0 \cdot (1+slack)$. For instance, the value could be $slack=0.1$,
representing 10\% allowed belt slack.

\textbf{Assumption 6: Constraining the length of the belt close to the natural length will avoid stretching the belt excessively, and will keep the belt taut.}

\subsubsection{Finding Belt Segments}
The state of the belt, i.e. which pulleys is it attached to and which line segments it consists of, is determined
purely by the position of the gripper. This analysis is performed in the
xy plane of the task frame, which is the same plane as that which the pulleys
line up on. The goal of this analysis is a list of all belt segments, each
described as two vectors, one for the start point of the segment $bs_{j,O}$ and another
as the size and direction of the belt segment $bs_{j,B}$. In any state,
the number of belt segments is denoted $m$, corresponding to the amount of pulleys
the belt is touching plus one. Figure
\ref{fig:belt-segments-example} shows a configuration and its associated belt
segments. Note that belt segments have a direction, going clockwise.
In general, the $j$th segment goes to the $j$th pulley, except the last segment
which goes to the gripper.

\textbf{Assumption 7: The state of the belt can be uniquely defined by the position of the gripper.}

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/belt-segments-example.jpg}
       }}
    \caption{State with three belt segments including direction.}
    \label{fig:belt-segments-example}
\end{figure}

The procedure for finding the belt segments can be seen in Figure
\ref{fig:find-belt-segments-pseudo}, where the functions \texttt{CPTangent} and
\texttt{CCTangent} finds the tangent line between a circle through a point, and
the tangent line between two circles respectively. Their implementation is
based on \cite{gboffi:cptangent} and \cite{ambrsoft:cctangent}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/find-belt-segments.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Algorithm \texttt{FindBeltSegments} for finding a list of
    belt segments in a given state.}
    \label{fig:find-belt-segments-pseudo}
\end{figure}

\subsection{Path Planning}
\label{sec:path_planning}
For path planning, two different planners are used. For the first
part of the task which features simple movements, a typical RRTConnect is used.
The second part uses a modefied RRTConnect which constrains the sampled
configurations due to the constraints imposed by the belt.

\subsubsection{RRTConnect}
RRTConnect is a bi-directional variant of the Rapidly-exploring Random Tree
(RRT) path planning algorithm, proposed by Kuffner et al. \cite{844730}.
RRTConnect finds a configuration space path
between a given start and goal configuration, by creating two trees with
roots in those configurations. By randomly sampling a new configuration
within the robot's joint limits, it examines a new point extended
a given distance $\epsilon$ from the nearest point in the tree.
If this point is not in collision, it is added to the tree with the nearest
point as parent. After every addition to the trees, if it is also within the
$\epsilon$ distance of the other tree, a connection is made and the
path between the start and goal configurations is extracted. The algorithms
has an iteration limit $i_{max}$ at which the planning is aborted.

There are many path planning algorithms, but RRT works well for simple, holonomic
systems, where movement is possible in all directions in all states, which
is true for robot manipulators with six degrees of freedom, as is the case
for the Univeral Robot robot used in this project. If a more advanced model of the
belt is used, such as the mass-spring model, the state of the system cannot
be determined purely by the state of the robot. This implies not only that the
representation of the state must be expanded to include the model, but that the dynamics
of the system becomes underactuated. The RRT is unable to work with such systems,
and another planner must be used, such as a kinodynamic one.

Figure \ref{fig:rrt-pseudo} shows pseudocode for the RRTConnect algorithm.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/rrt.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for the Rapidly-exploring Random Tree algorithm.}
    \label{fig:rrt-pseudo}
\end{figure}

\subsubsection{RRTConnect with First-order Retraction}
In order to adhere to the constraints imposed by the belt, the
RRTConnect algorithm is modified using first-order retraction (FR) based
on Stilman \cite{4399305}.
This allows the planner to adjust sampled configurations, and grow
the trees with only configurations that satisfy the constraints. It is possible to simply
discard any sampled configuration which do not adhere to the constraints, but it
is much faster to correct them, and can be done to any arbritary precision $err_{max}$.

Figure \ref{fig:fr-example} illustrates how this modified planner works.
The node denoted $q_s$ in the regular RRT is here denoted $q_r$, and FR is
performed to correct the node which gives the $q_s$ node. In the figure,
the red line represents the constraints and only configurations close to it
are allowed. The connected black dots is one of the two trees of the RRTConnect.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.5\textwidth}{
    \includegraphics[width=0.5\textwidth]{figures/fr-example.png}
       }}
    \caption{Example RRT iteration with first-order retraction. The red line indicates where the constraints allow nodes to be.}
    \label{fig:fr-example}
\end{figure}

The pseudocode for this modified RRTConnect can be seen in Figure
\ref{fig:rrt-fr-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/rrt-fr.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for the Rapidly-exploring Random Tree algorithm.}
    \label{fig:rrt-fr-pseudo}
\end{figure}

The \texttt{FRConstrain} function used in the pseudocode attempts to iteratively
correct the sampled configuration using the robot's geometric jacobian and
a 'task space error' which describes how the gripper should move in order
to adhere to the belt's constraints. Since the corrected node $q_s$ can be further
from $q_{near}$ then $q_r$, it is possible to connect two nodes which are
larger than the extension distance $\epsilon$ away from eachother. It is
possible to discard the sampled node if the distance is greater than $\epsilon$,
but as the correction is not guaranteed to be in any specific direction, there
is a great chance for this to happen, even if the correction is very small.
Therefore, a threshold of $1.5\epsilon$ limits how far any corrected point may
be from the nearest node. Importantly, this means that the largest distance between
any two points isn't $\epsilon$, but $1.5\epsilon$.

Since FR uses the jacobian, if the node is close to a singularity, the
correcting movement is likely to be very large, and the iterative method will
diverge away from the desired state. It uses the distance to $q_{near}$ to
determine this.
The pseudocode for \texttt{FRConstrain} can be seen in Figure
\ref{fig:frconstrain-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/frconstrain.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for \texttt{FRConstrain}.}
    \label{fig:frconstrain-pseudo}
\end{figure}

The jacobian used is found by calculating the geometric jacobian for
$T_l$ with respect to the world frame $J^0$.
It is then transformed into the task frame by using the rotation matrix part of
the task frame transformation $F_T$.

\begin{equation}
    J^t =
    \begin{bmatrix}
        F_{T,R} & 0\\
        0 & F_{T,R}
    \end{bmatrix}
    J^0
\end{equation}

The lower three rows of $J^0$ and $J^t$ are angular velocities. These
velocities do not map to instantaneous changes to any parameters used to
represent orientation. Therefore, given a configuration $q$, the jacobian
is further transformed with the $E(q)$ matrix which changes the
angular velocities of $J^t$ to instantaneous velocities.

\begin{equation}
    J(q) = E(q)J^t(q)
\end{equation}

For transforming the angular velocities to XYZ-Euler instantaneous velocities,
the $E(q)$ matrix is formed as seen in Equation \ref{eq:E-matrix}.

\begin{equation}
    \label{eq:E-matrix}
    E(q) =
    \begin{bmatrix}
        I_{3x3} & 0 & 0 & 0\\
        0 & c_{\phi}/c_{\theta} & s_{\phi}/c_{\theta} & 0 \\
        0 & -s_{\phi} & c_{\phi} & 0 \\
        0 & c_{\phi}s_{\theta}/c_{\theta} & s_{\phi}s_{\theta}/c_{\theta} & 1
    \end{bmatrix}
\end{equation}

The approach to finding this jacobian is also based on Stilman \cite{4399305}.

The \texttt{TaskSpaceError} function finds the translational and orientation
error $err$ for the left side of the gripper in task-space $T_l$.
Orientation is described as XYZ-Euler angles.
The x and y error adjusts the length of the belt, and the z error the height
of the end-effector. As the left side of the gripper should remain at $z=0$
the z error is simply the z component of the translational part $T_{l,z}$.
The x and y error is found by finding the length of the belt and how much it
should be adjusted, accounting for slack, and pulling it in an outwards
direction if the belt is too loose, or inwards if too stretched.

In order to approximate the corrective movement of the gripper,
the amount which the belt length $l$ exceeds the natural belt length $l_0$, accounting for slack,
is found and denoted as $dl$. This value is positive for belt lengths too great, and
negative for belt length too short.
A $bl$ vector goes from where the first belt segment touches the first pulley to
where the $m$th belt segment touches the $m$th pulley. The $bl$ vector is used to
define the $bd$ vector which is the direction in which the gripper is moved to
corrent the belt length. A right-angled triangle is formed with base $b=|bl|/2$ and
hypotenuse $c=(|bs_{1,B}|+|bs_{m,B}|)/2$.
Figure \ref{fig:triangle} illustrates this triangle.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.7\textwidth}{
    \includegraphics[width=0.7\textwidth]{figures/triangle.jpg}
       }}
    \caption{Example belt and pulley system with belt attached around all pulleys.}
    \label{fig:triangle}
\end{figure}

The derivative of the hypotenuse $c$ with respect to the height $a$ is used
to control the magnitude of the corrective movement.

\begin{equation}
    \frac{dc}{da} = \frac{a}{\sqrt{a^2+b^2}}
\end{equation}

The corrective movement for the gripper is found using the belt length error $dl$
and the derivative.

\begin{equation}
    P_{err} = \frac{bd \cdot dl \frac{a}{\sqrt{a^2+b^2}}}{2}
\end{equation}

The approximation may not be accurate in all cases, but because it is used in
an iterative algorithm, the important quiality is that it gives approximately the
correct movement.

The angular errors around the x and y axes are not corrected,
but the angle around the z axis is kept within $\SI{\pm0.1}{rad}$
of the average of the two connected belt segments. While in theory not
necessary to find a path, this reduces the search space of the planner,
increasing search speed drastically. Without including this error, the planner
is unable to find any path. The optimized path for solving the task is likely to
closely follow this restriction regardless.

Choosing not to correct the x and y axes allows for more free movement,
which may be necessary depending on the collision environment.

Pseudocode for the \texttt{TaskSpaceError} can be seen in Figure
\ref{fig:task-space-error-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/task-space-error.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for \texttt{TaskSpaceError}.}
    \label{fig:task-space-error-pseudo}
\end{figure}

\subsubsection{Path Optimization}
The path found using the planning methods are likely of much greater length than is possible.
For the paths during the first part of the task, that
which attaches the belt to the first pulley and pulls taut, the
shortcut algorithm is used. Each iteration, shortcut randomly finds two nodes
in the given path
and attempts to create a linearly interpolated shortcutting path between them.
If none of the interpolated configurations are in collision, the previous path
between those nodes are replaced by the interpolated path.
Given many iterations, the path length converges.
Pseudocode for the shortcut algorithm can be seen in Figure
\ref{fig:shortcut-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/shortcut.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for the shortcutting algorithm.}
    \label{fig:shortcut-pseudo}
\end{figure}

For the second part of the task, where the belt introduces constraints,
the linear interpolation cannot be used as is. The interpolation may create
new nodes that does not adhere to the constraints. Since both end points of
the interpolation do adhere to the constraints, the interpolated points are
likely to be close as well. Therefore FR can be used to correct the points.
For the second part, a slightly modified shortcut algorthim is proposed,
whose pseudocode can be seen Figure \ref{fig:shortcut-belt-pseudo}. The
method is modified so that for all nodes in any interpolated path, the node
gets corrected using \texttt{FRContrain}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/shortcut-belt.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Pseudocode for the shortcut algorithm modified using first-order retraction.}
    \label{fig:shortcut-belt-pseudo}
\end{figure}

The path optimization converges towards a path that narrowly avoids collision
with the pulleys. This lowers the maximum amount the belt is strained by
pulling, as such a path doesn't pull more than necessary. This means that
the $slack$ value introduced to express the allowed strain on the belt
may be larger than what the physical belt can tolerate, as the path will
be optimized to avoid pulling that far anyway. A higher $slack$ can be used
to more easily find a path, as a very strict $slack$ may leave the allowed
configuration space very narrow.

\subsection{Belt Collision Detection}
The belt segment attached to the right side of the gripper $T_r$, must be ensured
to not collide with the pulleys. Therefore, along with the typical collision
detection between the robot and the environment, any state is checked for
collisions between this belt segment and all pulleys.
The collision check between a belt segment and a pulley is found with simple
geometric analysis. To find collisions, $20$ points are linearly interpolated along the belt segment. All points may
not be inside a cylinder centered in the pulley with the pulley's radius and height.
The cylinder extends infinitely downwards, as it is never desired for a belt
segment to be below a pulley. Pseudocode for finding collisions between any
pulley can be seen in Figure \ref{fig:belt-collision-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/belt-collision.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Procedure for finding the collisions between a belt segment and pulleys.}
    \label{fig:belt-collision-pseudo}
\end{figure}

\subsubsection{Joint Weights}
\label{sec:joint-weights}
The different joints represent varying sensitivity to the end effector for a robot manipulator.
For instance, the position of the end effector is much more sensitive to the
changes in the first joint compared to the rest. For the specific path planning
in this project, the last joint, that which controls the z axis of teh end effector,
is to be moved much more than the rest, and
to 'encourage' the planner's movement for the last joint, joint weights are
implemented when calculating configuration space distance. The last joint is
given a weight of $0.2$, with other joints remaining at $1$.
This is a rather simple modification which aims to reduce planning time,
and to let the path optimization algorithms prioritize pruning the
first five joints.

\subsection{Creating the Path}
This section will desribe how the waypoints between which the two types of
path planning is performed, and how the path is joined.

The first part of the task, attaching the belt and pulling taut, is intended
to be a rather simple movement that does not consider the state of the belt,
and assumes the performed movement will attach it correctly.

When the belt is pulled taut, it will extend further from the first pulley
than the distance to any other pulley, so the direction in which it will be pulled
must be chosen as to not collide with any pulley. As the belt will be attached clockwise in task
space, the belt will be pulled in an angle $\alpha$ just to the 'left' of the second
pulley in task space. The value of this angle is not critical, and is
chosen to be $\alpha=\SI{30}{deg}$.

The path for the first part is found using five configuration space waypoints $wp_n$.
The first waypoint is a little above the first pulley such that the following
approach ensures the belt goes around the pulley. The second waypoint is
directly below the first, and is at the same height as the pulley. The third
waypoint is rotated such that the gripper aligns on a tangent of the pulley,
and the belt enters the pulley's indent. The fourth waypoint pulls the belt
away from the pulley along the angle of the tangent such that the belt stays
in the indent. The fifth waypoint rotates the gripper such that the angle
of the gripper is close to that allowed by the task space constraints. Figure
\ref{fig:waypoints-picture} shows the waypoints.

\begin{figure}[!ht]
    \centering
    \framebox{\parbox{0.7\textwidth}{
    \includegraphics[width=0.7\textwidth]{figures/waypoints-picture.jpg}
       }}
    \caption{The five waypoints which the first part of the task consists of.}
    \label{fig:waypoints-picture}
\end{figure}

The second part has only a single start and end configuration, and uses the
fifth waypoint as the starting position and calculates a sixth waypoint
as the end configuration. The fifth and sixth waypoint is corrected using
\texttt{FRConstrain} to ensure that they are valid configurations for the
modified path planning algorithm.

The waypoints are found by calculating a corresponding transformation $wpT_n$ and using
inverse kinematics to find the waypoint in configuration space.
It is natural to calculate the waypoint transformations out of order. The calculations use
the methods $transl(x,y,z)$ which creates a transformation matrix with that
translation, and $trotx(\psi)$, $troty(\theta)$ and $trotz(\phi)$ which
creates transformation matrices that rotates with the given angle. After the
waypoint transformations are found, they are rotated $\pi$ around the x axis as
to align the transformation with how the gripper's transformation is rotated.
The inverse kinematics is calculated by analytically finding all possible configurations
for a given waypoint transformation, and picking that which is closest to the previous waypoint.
The first waypoint is found by choosing the solution nearest to the robot's home
configuration $q_{home}$
The math for finding the waypoints can be seen in Figure \ref{fig:waypoints-pseudo}.

\begin{figure}[H]
    \centering
    \framebox{\parbox{5.5in}{
        \begin{minipage}{5.5in}
        \input{algorithms/waypoints.tex}
        \end{minipage}
    }}
    \captionsetup{width=5.5in}
    \caption{Procedure for finding the waypoints.}
    \label{fig:waypoints-pseudo}
\end{figure}

The solution for finding these waypoint transformations is neither elegant or smart. There
may very easily be setups in which this approach needs to be modified, but
it works with those used in this project. The reader is advised only to inspect
it if interested, otherwise observing
the illustrations of the waypoints in Figure \ref{fig:waypoints-picture} should be
sufficient for understanding their placements.

Between the robot's home configuration and through waypoint 1 to 5, the standard
RRTConnect and shortcut are used to plan and optimize the path. From waypoint
5 to 6, the modified RRTConnect and shortcut are used.

\subsection{Validation}
When the RRT algorithm makes a link between two collision-free nodes, there is
no guarantee that the configurations between the nodes are collision free, and
that the path is possible for the robot to perform. To lessen this problem,
it is possible to use collision objects that are slightly larger than the
objects in the environment that they represent. These inflated collision
objects are used in both the path planning and path optimization.

When the path
is created, a check is performed on the path to attempt to ensure it wouldn't
produce collisions if performed. The check interpolates points between every
two nodes along the path with a maximum configuration space distance of $0.02$
between any interpolated points. If any point is in collisions the check fails.
The degree to which the check corresponds to whether the path is actually able
to be performed is not known or tested for. However it succeeds at failing paths
created with high extension value $\epsilon$ which jumps across
collision objects. The check is not performed with inflated collision objects.

\subsection{Implementation}
The implementation is developed in MATLAB and can be found in the Digital
Appendix or on
\href{https://www.gitlab.com/sampuka/masters-elastic-assembly}{GitLab}\footnote{\url{https://www.gitlab.com/sampuka/masters-elastic-assembly}. The code base is large and not well written, and is probably difficult to read.}.

The robotics environment used to visualize the work space, compute collision
checks and more is MATLAB's built-in Robotics Systems Toolbox. The
robot is defined using a URDF-file which can describe all joints, links, visual
models and collision models for a robot.
Regarding collision, the pulleys in the environment is modeled as cylinders.
All other collision features needed to represent a physical environment can be easily included.

The implementation of the path planning and optimization algorithms proposed
in Section \ref{sec:path_planning} are as described except for a few changes.
The search tree data structure used in the path planing algorithms have
built-in, non-editable \texttt{NearestNeighbor} which does not consider the
joint weights.
An attempt at replacing this data structure with something that does, introduced
inefficiencies that greatly slowed computations.

The implementation for the analytical inverse kinematics is based on \cite{rasmus:ur5ik}.

\subsubsection{Robot Control}
To perform a path on the UR10e robot, the RTDE protocol is used. The
protocol includes a \texttt{servoJ} command which can be used to control
the state of the robot in configuration space. The \texttt{servoJ} command
takes a target configuration and a time period in which the movement
needs to be completed. The command can be used for each individual step in
a discrete path, with a time period calculated using the distance for the step,
and a desired configuration space velocity. A Python script is written to
send the commands.
The gripper is controlled using the RobotiqGripper interface.
